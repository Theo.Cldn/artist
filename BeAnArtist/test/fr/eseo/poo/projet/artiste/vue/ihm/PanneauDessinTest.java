package fr.eseo.poo.projet.artiste.vue.ihm;

import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import java.awt.Color;


public class PanneauDessinTest {

	public  PanneauDessinTest() {
		testConstructeurParDefaut();
		testConstructeur();
	}
	
	private void testConstructeurParDefaut(){
		JFrame fenetre1 = new JFrame("Etre un Artiste");
		fenetre1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre1.setLocationRelativeTo(null);
		fenetre1.setSize(400,240);
		
		PanneauDessin panneauDessin1 = new PanneauDessin();
		fenetre1.setContentPane(panneauDessin1);
		fenetre1.setVisible(true);
	}
	
	private void testConstructeur() {
		JFrame fenetre2 = new JFrame("Blues du Businessman");
		fenetre2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre2.setLocationRelativeTo(null);
		fenetre2.setSize(400,240);
		
		PanneauDessin panneauDessin2 = new PanneauDessin(800, 700, Color.ORANGE);
		fenetre2.setContentPane(panneauDessin2);
		fenetre2.setVisible(true);
	}
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				new PanneauDessinTest();
			}
		});
	}
	
}
