package fr.eseo.poo.projet.artiste.vue.ihm;

import javax.swing.SwingUtilities;
import javax.swing.JFrame;

import java.awt.BorderLayout;


public class PanneauBarreOutilsTest {
	
	public PanneauBarreOutilsTest() {
		testPanneauBareOutil();
	}
	
	
	private void testPanneauBareOutil(){
		JFrame fenetre1 = new JFrame("Etre un Artiste");
		fenetre1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre1.setLocationRelativeTo(null);
		fenetre1.setLayout(new BorderLayout());
		
		
		PanneauDessin panneauDessin = new PanneauDessin();
		
		PanneauBarreOutils panneauBarreOutils= new PanneauBarreOutils(panneauDessin);
		
		fenetre1.add(panneauDessin, BorderLayout.CENTER);
		fenetre1.add(panneauBarreOutils, BorderLayout.EAST);
		
		
		fenetre1.setSize(700,400);
		fenetre1.setVisible(true);
	}
	
	
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				new PanneauBarreOutilsTest();
			}
		});
	}


}
