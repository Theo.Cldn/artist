package fr.eseo.poo.projet.artiste.vue.formes;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

public class VueCercleTest {
	
	public  VueCercleTest() {
		testVueCercle();
	}
	
	private void testVueCercle(){
		JFrame fenetre = new JFrame("Etre un Artiste");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setLocationRelativeTo(null);
		fenetre.setSize(400,240);
		
		PanneauDessin panneauDessin = new PanneauDessin();
		fenetre.setContentPane(panneauDessin);
		
		Coordonnees c1= new Coordonnees(1,1);
		Cercle cl1= new Cercle(c1,50.0);
		Coordonnees c2= new Coordonnees(7,7);
		Cercle cl2= new Cercle(c2,30.0);
		VueCercle vc1 = new  VueCercle(cl1);
		VueCercle vc2 = new  VueCercle(cl2);
		System.out.println(vc1.getForme());
		System.out.println(vc2.getForme());
		panneauDessin.ajouterVueForme(vc1);
		panneauDessin.ajouterVueForme(vc2);
		
		fenetre.setVisible(true);
		
	}
	
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				new VueCercleTest();
			}
		});
	}
	
}
