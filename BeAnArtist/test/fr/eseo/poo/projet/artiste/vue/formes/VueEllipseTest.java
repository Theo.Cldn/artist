package fr.eseo.poo.projet.artiste.vue.formes;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
//import java.awt.Color;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

public class VueEllipseTest {
	
	public  VueEllipseTest() {
		testVueEllipse();
	}
	
	private void testVueEllipse(){
		JFrame fenetre = new JFrame("Etre un Artiste");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setLocationRelativeTo(null);
		fenetre.setSize(400,240);
		
		PanneauDessin panneauDessin = new PanneauDessin();
		fenetre.setContentPane(panneauDessin);
		
		Coordonnees c1= new Coordonnees(1,1);
		Ellipse e1= new Ellipse(c1,50.0,20.0);
		Coordonnees c2= new Coordonnees(7,7);
		Ellipse e2= new Ellipse(c2,30.0,50.0);
		VueEllipse ve1 = new  VueEllipse(e1);
		VueEllipse ve2 = new  VueEllipse(e2);
		System.out.println(ve1.getForme());
		System.out.println(ve2.getForme());
		panneauDessin.ajouterVueForme(ve1);
		panneauDessin.ajouterVueForme(ve2);
		
		fenetre.setVisible(true);
		
	}
	
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				new VueEllipseTest();
			}
		});
	}
	
}
