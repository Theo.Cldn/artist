package fr.eseo.poo.projet.artiste.vue.formes;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
//import java.awt.Color;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;


public class VueLigneTest {

	public  VueLigneTest() {
		testVueLigne();
	}
	
	private void testVueLigne(){
		JFrame fenetre = new JFrame("Etre un Artiste");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setLocationRelativeTo(null);
		fenetre.setSize(400,240);
		
		PanneauDessin panneauDessin = new PanneauDessin();
		fenetre.setContentPane(panneauDessin);
		
		Coordonnees c1= new Coordonnees(1,1);
		Ligne l1= new Ligne(c1,50.0,20.0);
		Coordonnees c2= new Coordonnees(7,7);
		Ligne l2= new Ligne(c2,30.0,50.0);
		VueLigne vl1 = new  VueLigne(l1);
		VueLigne vl2 = new  VueLigne(l2);
		System.out.println(vl1.getForme());
		System.out.println(vl2.getForme());
		panneauDessin.ajouterVueForme(vl1);
		panneauDessin.ajouterVueForme(vl2);
		
		fenetre.setVisible(true);
		
	}
	
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				new VueLigneTest();
			}
		});
	}
	
}
