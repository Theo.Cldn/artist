package fr.eseo.poo.projet.artiste.modele.formes;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.awt.Color;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;


public class CercleTest {

	@Test  //Test 4 Constructeurs
	public void testConstrusteur1() {
		Cercle c1 = new Cercle();
		
		assertEquals("Position Attendue","(0,0 , 0,0)", c1.getPosition().toString());
		assertEquals("Largeur Attendue",10.0, c1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",10.0,c1.getHauteur(),10e-2);
		assertEquals("Resultat Attendu", c1.getLargeur(), c1.getHauteur(),10e-2);
	}
	
	@Test
	public void testConstrusteur2() {
		Cercle c1 = new Cercle(2.0);
		
		assertEquals("Position Attendue","(0,0 , 0,0)", c1.getPosition().toString());
		assertEquals("Largeur Attendue",2.0, c1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",2.0,c1.getHauteur(),10e-2);
		assertEquals("Resultat Attendu", c1.getLargeur(), c1.getHauteur(),10e-2);
	}
	
	@Test
	public void testConstrusteur3() {
		Coordonnees p1= new Coordonnees(5.0, 7.0);
		Cercle c1 = new Cercle(p1);
		
		assertEquals("Position Attendue","(5,0 , 7,0)", c1.getPosition().toString());
		assertEquals("Largeur Attendue",10.0, c1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",10.0,c1.getHauteur(),10e-2);
		assertEquals("Resultat Attendu", c1.getLargeur(), c1.getHauteur(),10e-2);
	}
	
	@Test
	public void testConstrusteur4() {
		Coordonnees p1= new Coordonnees(5.0, 7.0);
		Cercle c1 = new Cercle(p1, 7.0);
		
		assertEquals("Position Attendue","(5,0 , 7,0)", c1.getPosition().toString());
		assertEquals("Largeur Attendue",7.0, c1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",7.0, c1.getHauteur(),10e-2);
		assertEquals("Resultat Attendu", c1.getLargeur(), c1.getHauteur(),10e-2);
	}
	
	
	//Test Exception Constructeur		
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionTailleConstructeur() throws Exception {
		Coordonnees c1= new Coordonnees(5.0, 7.0);
		new Cercle(c1, -7.0);		
	}
	
		
	//Test Exception setLargeur et setHauteur
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetLargeur() throws Exception {
		Coordonnees p1= new Coordonnees(5.0, 7.0);
		Cercle c1= new Cercle(p1, 7.0);
		c1.setLargeur(-7.0);
	}
		
		
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetHauteur() throws Exception {
		Cercle c1= new Cercle(7.0);	
		c1.setHauteur(-8.0);
	}
	
	
	@Test  //Test set
	public void testSetLargeur() {
		Cercle c1 = new Cercle();
		c1.setLargeur(30.0);
		
		assertEquals("Largeur Attendue",30.0, c1.getLargeur(),10e-2);
	}

	@Test
	public void testSetHauteur() {
		Cercle c1 = new Cercle();
		c1.setHauteur(30.0);
		
		assertEquals("Hauteur Attendue",30.0, c1.getHauteur(),10e-2);
	}
	

	@Test  //Test aire
	public void testAire() {
		Coordonnees p1 = new Coordonnees(5.0,7.0);
		Cercle c1 = new Cercle(p1,5.0);
		assertEquals("Aire Attendue",19.63,c1.aire(),10e-2);
	}
			
	
	@Test  	//Test perimetre
	public void testPerimetre() {
		Coordonnees p1 = new Coordonnees(5.0,7.0);
		Cercle c1 = new Cercle(p1,2.0);		
		assertEquals("Perimetre Attendu",6.28,c1.perimetre(),10e-2);
	}
		
	
	@Test  //Test toString
	public void testToString() {
		Coordonnees c1= new Coordonnees(10.0, 10.0);
		Cercle c2 = new Cercle(c1, 25.0);
		
		assertEquals("toString Attendu","[Cercle] : pos (10,0 , 10,0) dim 25,0 x 25,0 "+
		"périmètre : 78,54 aire : 490,87 couleur = "+c2.toStringCouleur(),c2.toString());
		c2.setRempli(true);
		assertEquals("toString Attendu","[Cercle-Rempli] : pos (10,0 , 10,0) dim 25,0 x 25,0 "+
				"périmètre : 78,54 aire : 490,87 couleur = "+c2.toStringCouleur(),c2.toString());
	}

	
	@Test  //Test contient
	public void testContient() {
		Coordonnees c1= new Coordonnees(0, 10.0);
		Coordonnees c2= new Coordonnees(5.0, 10.0);
		Coordonnees c3= new Coordonnees(0.0, 10.0);
		Cercle e1 = new Cercle(c1, 10.0);
		
		assertEquals(true, e1.contient(c2));
		assertEquals(false, e1.contient(c3));
	}
	

	@Test  	//Test couleur
	public void testCouleur() {
		Coordonnees c1= new Coordonnees(50.0, 50.0);
		Cercle c2 = new Cercle(c1,20.0);
		
		assertEquals("Couleur Attendue",Forme.COULEUR_PAR_DEFAUT,c2.getCouleur());
		c2.setCouleur(Color.RED);
		assertEquals("Couleur Attendue",Color.RED,c2.getCouleur());
	}
}
