package fr.eseo.poo.projet.artiste.modele;

import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class CoordonneesTest {

	@Test
	public void testConstructeur1() {
		Coordonnees coordonnees = new Coordonnees(7.0,8.0);
		assertEquals("Abscisse Attendue",7.0,coordonnees.getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",8.0,coordonnees.getOrdonnee(),10e-2);
	}

	@Test
	public void testConstructeur2() {
		Coordonnees coordonnees = new Coordonnees();
		assertEquals("Abscisse Attendue",0.0,coordonnees.getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",0.0,coordonnees.getOrdonnee(),10e-2);
	}
	
	
	@Test
	public void testDeplacerDe() {
		Coordonnees coord1 = new Coordonnees(7.0,8.0);
		coord1.deplacerDe(1.0,2.0);
		
		assertEquals("Abscisse Attendue",8.0,coord1.getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",10.0,coord1.getOrdonnee(),10e-2);
	}
	
	
	@Test
	public void testDeplacerVers() {
		Coordonnees coord1 = new Coordonnees(7.0,8.0);
		coord1.deplacerVers(1.0,2.0);
		
		assertEquals("Abscisse Attendue",1.0,coord1.getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",2.0,coord1.getOrdonnee(),10e-2);
	}
	
	
	
	@Test
	public void testDistanceVers() {
		Coordonnees coord1 = new Coordonnees();
		Coordonnees coord2 = new Coordonnees(2.0,2.0);
		
		assertEquals("Distance Attendue",Math.sqrt(8.0),coord1.distanceVers(coord2),10e-2);
	}
	
	
	@Test
	public void testAngleVers() {
		Coordonnees coord1 = new Coordonnees();
		Coordonnees coord2 = new Coordonnees(2.0,2.0);
		
		assertEquals("Angle Attendu",Math.atan(1.0),coord1.angleVers(coord2),10e-2);
	}
	
	
	@Test
	public void testToString() {
		Coordonnees coord1 = new Coordonnees(7.0,8.0);
		
		assertEquals("Coordonnee Attendue","(7,0 , 8,0)",coord1.toString());
	}
	
}
