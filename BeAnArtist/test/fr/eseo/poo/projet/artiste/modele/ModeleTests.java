package fr.eseo.poo.projet.artiste.modele;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.eseo.poo.projet.artiste.modele.formes.CercleTest;
import fr.eseo.poo.projet.artiste.modele.formes.CercleTestParameterized;
import fr.eseo.poo.projet.artiste.modele.formes.EllipseTest;
import fr.eseo.poo.projet.artiste.modele.formes.EtoileTest;
import fr.eseo.poo.projet.artiste.modele.formes.LigneTest;
import fr.eseo.poo.projet.artiste.modele.formes.LigneTestParameterized;

@RunWith(Suite.class)
@SuiteClasses({ CoordonneesTest.class,  LigneTest.class, CercleTest.class, 
				EllipseTest.class, EtoileTest.class, LigneTestParameterized.class , CercleTestParameterized.class})

public class ModeleTests {

}
