package fr.eseo.poo.projet.artiste.modele.formes;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;


@RunWith(Parameterized.class)
public class CercleTestParameterized {

	private double abscisse;
	private double ordonnee;
	private boolean contient;
	
	
	public CercleTestParameterized(double abscisse, double ordonnee, boolean contient) {
		this.abscisse= abscisse;
		this.ordonnee= ordonnee;
		this.contient= contient;
	}

	
	@Parameters(name = "dt[{index}] : {0}, {1}, {2}") 
    public static Collection<Object[]> dt() {
        Object[][] data = new Object[][] { 
        	{0.0, 0.001, false},
        	{0.501, 0.5, false},
    		{1.0, 1.001, false},
    		{1.501, 1.5, true},
    		{2.0, 2.001, true},
    		{2.501, 2.5, true},
    		{3.0, 3.001, true},
    		{3.501, 3.5, true},
    		{4.0, 4.001, true},
    		{4.501, 4.5, true},
    		{5.0, 5.001, true},
    		{5.501, 5.5, true},
    		{6.0, 6.001, true},
    		{6.501, 6.5, true},
    		{7.0, 7.001, true},
    		{7.501, 7.5, true},
    		{8.0, 8.001, true},
    		{8.501, 8.5, true},
    		{9.0, 9.001, false},
    		{9.501, 9.5, false},
    		{10.0, 10.001, false}
    		
    		
        };
        return Arrays.asList(data);
    }
    
 
    @Test
	public void testgetType() {
    	Coordonnees c1= new Coordonnees(0.0,0.0);
		Cercle cercle = new Cercle(c1,10.0);

		Coordonnees coordonnees= new Coordonnees(abscisse, ordonnee);
		assertEquals("Test Contient", contient, cercle.contient(coordonnees));
	}

}
