package fr.eseo.poo.projet.artiste.modele.formes;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.awt.Color;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;


public class EllipseTest {

	@Test  //Test 4 constructeurs
	public void testConstrusteur1() {
		Ellipse e1 = new Ellipse();
		
		assertEquals("Position Attendue","(0,0 , 0,0)", e1.getPosition().toString());
		assertEquals("Largeur Attendue",10.0, e1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",10.0,e1.getHauteur(),10e-2);
	}
	
	@Test
	public void testConstrusteur2() {
		Ellipse e1 = new Ellipse(2.0, 3.0);
		
		assertEquals("Position Attendue","(0,0 , 0,0)", e1.getPosition().toString());
		assertEquals("Largeur Attendue",2.0, e1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",3.0,e1.getHauteur(),10e-2);
	}
	
	@Test
	public void testConstrusteur3() {
		Coordonnees c1= new Coordonnees(5.0, 7.0);
		Ellipse e1 = new Ellipse(c1);
		
		assertEquals("Position Attendue","(5,0 , 7,0)", e1.getPosition().toString());
		assertEquals("Largeur Attendue",10.0, e1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",10.0,e1.getHauteur(),10e-2);
	}
	
	@Test
	public void testConstrusteur4() {
		Coordonnees c1= new Coordonnees(5.0, 7.0);
		Ellipse e1 = new Ellipse(c1, 7.0, 8.0);
		
		assertEquals("Position Attendue","(5,0 , 7,0)", e1.getPosition().toString());
		assertEquals("Largeur Attendue",7.0, e1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",8.0,e1.getHauteur(),10e-2);
	}
	
	
	//Test Exception Constructeur
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionLargeurConstructeur() throws Exception {
		Coordonnees c1= new Coordonnees(5.0, 7.0);
		new Ellipse(c1, -7.0, 8.0);		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionHauteurConstructeur() throws Exception {
		Coordonnees c1= new Coordonnees(5.0, 7.0);
		new Ellipse(c1, 7.0, -8.0);		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionDeuxConstructeur() throws Exception {
		Coordonnees c1= new Coordonnees(5.0, 7.0);
		new Ellipse(c1, -7.0, -8.0);		
	}
	
	
	//Test Exception setLargeur et setHauteur
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetLargeur() throws Exception {
		Coordonnees c1= new Coordonnees(5.0, 7.0);
		Ellipse e1 = new Ellipse(c1, 7.0, 8.0);
		e1.setLargeur(-7.0);
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetHauteur() throws Exception {
		Coordonnees c1= new Coordonnees(5.0, 7.0);
		Ellipse e1 = new Ellipse(c1, 7.0, 8.0);
		e1.setHauteur(-8.0);
	}
	
	
	@Test  //Test Set
	public void testSetLargeur() {
		Ellipse e1 = new Ellipse();
		e1.setLargeur(20.0);
		
		assertEquals("Largeur Attendue",20.0, e1.getLargeur(),10e-2);
	}
	
	@Test
	public void testSetHauteur() {
		Ellipse e1 = new Ellipse();
		e1.setHauteur(20.0);
		
		assertEquals("Hauteur Attendue",20.0, e1.getHauteur(),10e-2);
	}
	
	
	@Test  //Test aire
	public void testAire() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ellipse e1 = new Ellipse(c1,5.0,10.0);
			
		assertEquals("Aire Attendue",39.26,e1.aire(),10e-2);
	}
		

	@Test  //Test perimetre
	public void testPerimetre() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ellipse e1 = new Ellipse(c1,2.0,4.0);
				
		assertEquals("Perimetre Attendu",9.68,e1.perimetre(),10e-2);
	}
	

	@Test  //Test toString
	public void testToString() {
		Coordonnees c1= new Coordonnees(10.0, 10.0);
		Ellipse e1 = new Ellipse(c1, 25.0, 15.0);
		
		assertEquals("toString Attendu","[Ellipse] : pos (10,0 , 10,0) dim 25,0 x 15,0 "+
		"périmètre : 63,82 aire : 294,52 couleur = "+e1.toStringCouleur(),e1.toString());
		e1.setRempli(true);
		assertEquals("toString Attendu","[Ellipse-Rempli] : pos (10,0 , 10,0) dim 25,0 x 15,0 "+
				"périmètre : 63,82 aire : 294,52 couleur = "+e1.toStringCouleur(),e1.toString());
	}
	
	
	@Test  //Test contient
	public void testContient() {
		Coordonnees c1= new Coordonnees(100.0, 100.0);
		Ellipse e1 = new Ellipse(c1, 100.0, 100.0);
		Coordonnees c2= new Coordonnees(150.0, 150.0);
		Coordonnees c3= new Coordonnees(100.0, 200.0);
		
		assertEquals(true, e1.contient(c2));
		assertEquals(false, e1.contient(c3));
	}
	
	
	@Test  //Test couleur
	public void testCouleur() {
		Coordonnees c1= new Coordonnees(50.0, 50.0);
		Ellipse e1 = new Ellipse(c1,10.0,20.0);
		
		assertEquals("Couleur Attendue",Forme.COULEUR_PAR_DEFAUT,e1.getCouleur());
		e1.setCouleur(Color.RED);
		assertEquals("Couleur Attendue",Color.RED,e1.getCouleur());
	}
	
}
