package fr.eseo.poo.projet.artiste.modele.formes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;


public class EtoileTest {

	@Test
	public void testSetPosition() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1);
		e1.setPosition(new Coordonnees(0.0, 0.0));
		
		assertEquals("Abscisse Attendue",0.0, e1.getPosition().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",0.0, e1.getPosition().getOrdonnee(),10e-2);
	}

	@Test
	public void testSetLargeur() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15);
		e1.setLargeur(20.0);
		
		assertEquals("Largeur Attendue",20.0, e1.getLargeur(),10e-2);
	}

	@Test
	public void testSetHauteur() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15);
		e1.setHauteur(20.0);
		
		assertEquals("Hauteur Attendue",20.0, e1.getHauteur(),10e-2);
	}

	@Test
	public void testAire() {
	
	}

	@Test
	public void testPerimetre() {
		Coordonnees c1= new Coordonnees(0.0, 0.0);
		Etoile e1= new Etoile(c1, 2, 4, 0, 0.5);
		
		assertEquals("Perimetre Attendu", 5.895, e1.perimetre(),10e-2);
	}

	@Test
	public void testContient() {
	
	}

	@Test
	public void testEtoile() {
		Etoile e1= new Etoile();
		
		assertEquals("Abscisse Attendue",0.0, e1.getPosition().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",0.0, e1.getPosition().getOrdonnee(),10e-2);
		assertEquals("Largeur Attendue",10.0, e1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",10.0, e1.getHauteur(),10e-2);
		assertEquals("Nombre Branches Attendus",4, e1.getNombreBranches(),10e-2);
		assertEquals("Angle Premire Branche Attendu",Math.PI/2, e1.getAnglePremiereBranche(),10e-2);
		assertEquals("Longueur Branches Attendues",0.5, e1.getLongueurBranche(),10e-2);
	}

	@Test
	public void testEtoileDouble() {
		Etoile e1= new Etoile(15.0);
		
		assertEquals("Abscisse Attendue",0.0, e1.getPosition().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",0.0, e1.getPosition().getOrdonnee(),10e-2);
		assertEquals("Largeur Attendue",15.0, e1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",15.0, e1.getHauteur(),10e-2);
		assertEquals("Nombre Branches Attendus",4, e1.getNombreBranches(),10e-2);
		assertEquals("Angle Premire Branche Attendu",Math.PI/2, e1.getAnglePremiereBranche(),10e-2);
		assertEquals("Longueur Branches Attendues",0.5, e1.getLongueurBranche(),10e-2);
	}

	@Test
	public void testEtoileCoordonnees() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1);
		
		assertEquals("Abscisse Attendue",50.0, e1.getPosition().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",60.0, e1.getPosition().getOrdonnee(),10e-2);
		assertEquals("Largeur Attendue",10.0, e1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",10.0, e1.getHauteur(),10e-2);
		assertEquals("Nombre Branches Attendus",4, e1.getNombreBranches(),10e-2);
		assertEquals("Angle Premire Branche Attendu",Math.PI/2, e1.getAnglePremiereBranche(),10e-2);
		assertEquals("Longueur Branches Attendues",0.5, e1.getLongueurBranche(),10e-2);
	}

	@Test
	public void testEtoileCoordonneesDouble() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15);
		
		assertEquals("Abscisse Attendue",50.0, e1.getPosition().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",60.0, e1.getPosition().getOrdonnee(),10e-2);
		assertEquals("Largeur Attendue",15.0, e1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",15.0, e1.getHauteur(),10e-2);
		assertEquals("Nombre Branches Attendus",4, e1.getNombreBranches(),10e-2);
		assertEquals("Angle Premire Branche Attendu",Math.PI/2, e1.getAnglePremiereBranche(),10e-2);
		assertEquals("Longueur Branches Attendues",0.5, e1.getLongueurBranche(),10e-2);
	}

	@Test
	public void testEtoileCoordonneesDoubleIntDoubleDouble() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15, 10, Math.PI, 0.5);
		
		assertEquals("Abscisse Attendue",50.0, e1.getPosition().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",60.0, e1.getPosition().getOrdonnee(),10e-2);
		assertEquals("Largeur Attendue",15.0, e1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",15.0, e1.getHauteur(),10e-2);
		assertEquals("Nombre Branches Attendus",10, e1.getNombreBranches(),10e-2);
		assertEquals("Angle Premire Branche Attendu",Math.PI, e1.getAnglePremiereBranche(),10e-2);
		assertEquals("Longueur Branches Attendues",0.5, e1.getLongueurBranche(),10e-2);
	}
	
	
	//Test Exception Constructeur
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionLargeurConstructeur() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		new Etoile(c1, -2, 15, Math.PI, 1.0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionBranchesConstructeur1() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		new Etoile(c1, 2, 2, Math.PI, 2.0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionBranchesConstructeur2() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		new Etoile(c1, 2, 16, Math.PI, 2.0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionAngleConstructeur1() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		new Etoile(c1, 2, 5, 2*Math.PI, 2.0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionAngleConstructeur2() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		new Etoile(c1, 2, 5, -2*Math.PI, 2.0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionLongueurConstructeur1() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		new Etoile(c1, 2, 10, Math.PI, -2.0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionLongueurConstructeur2() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		new Etoile(c1, 2, 10, Math.PI, 2.0);
	}
	
	
	//Test Exception setLargeur et setHauteur
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetLargeur() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 2, 15, Math.PI, 1.0);
		e1.setLargeur(-2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetHauteur() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 2, 15, Math.PI, 1.0);
		e1.setHauteur(-2);
	}
	
	//Test Exception setNombreBranches
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetNbBranches() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 2, 15, Math.PI, 1.0);
		e1.setNombreBranches(2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetNbBranches2() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 2, 15, Math.PI, 1.0);
		e1.setNombreBranches(16);
	}
	
	//Test Exception setAnglePremiereBranche
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetAnglePreBranche() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 2, 15, Math.PI, 1.0);
		e1.setAnglePremiereBranche(-2*Math.PI);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetAnglePreBranche2() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 2, 15, Math.PI, 1.0);
		e1.setAnglePremiereBranche(2*Math.PI);
	}
	
	//Test Exception setLongueurBranche
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetLongueurBranche() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 2, 15, Math.PI, 1.0);
		e1.setLongueurBranche(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionSetLongueurBranche2() throws Exception {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 2, 15, Math.PI, 1.0);
		e1.setLongueurBranche(2);
	}
		
	//Autre test set
	@Test
	public void testSetNombreBranches() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15, 10, Math.PI, 0.5);
		e1.setNombreBranches(5);
		
		assertEquals("Nombre Branches Attendus",5, e1.getNombreBranches(),10e-2);
		
	}

	@Test
	public void testSetAnglePremiereBranche() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15, 10, Math.PI, 0.5);
		e1.setAnglePremiereBranche(Math.PI/2);
		
		assertEquals("Angle Premire Branche Attendu",Math.PI/2, e1.getAnglePremiereBranche(),10e-2);
	}

	@Test
	public void testSetLongueurBranche() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15, 10, Math.PI, 0.5);
		e1.setLongueurBranche(0.75);
		
		assertEquals("Longueur Branches Attendues",0.75, e1.getLongueurBranche(),10e-2);
	}

	@Test
	public void testGetCoordonnees() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15, 10, Math.PI, 0.5);
		
		assertEquals("Coordonnee Attendue","(50,0 , 60,0)", e1.getPosition().toString());
	}

	@Test
	public void testGetNombreBranches() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15, 10, Math.PI, 0.5);
		
		assertEquals("Coordonnee Attendue", 10, e1.getNombreBranches(),10e-2);
	}

	@Test
	public void testGetAnglePremiereBranche() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15, 10, Math.PI, 0.5);
		
		assertEquals("Coordonnee Attendue", Math.PI, e1.getAnglePremiereBranche(),10e-2);
	}

	@Test
	public void testGetLongueurBranche() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15, 10, Math.PI, 0.5);
		
		assertEquals("Coordonnee Attendue", 0.5, e1.getLongueurBranche(),10e-2);
	}

	@Test
	public void testToString() {
		Coordonnees c1= new Coordonnees(10.0, 10.0);
		Etoile e1= new Etoile(c1, 30, 10, Math.PI, 0.5);
		
		assertEquals("toString Attendu","[Etoile] : pos (10,0 , 10,0) dim 30,0 x 30,0 "+
		"périmètre : 164,03 aire : 347,64 couleur = "+e1.toStringCouleur(),e1.toString());
		e1.setRempli(true);
		assertEquals("toString Attendu","[Etoile-Rempli] : pos (10,0 , 10,0) dim 30,0 x 30,0 "+
		"périmètre : 164,03 aire : 347,64 couleur = "+e1.toStringCouleur(),e1.toString());
	}

	@Test
	public void testSetRempli() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15, 10, Math.PI, 0.5);
		
		assertEquals("Remplissage Attendu", false, e1.estRempli());
		e1.setRempli(true);
		assertEquals("Remplissage Attendu", true, e1.estRempli());
	}

	@Test
	public void testEstRempli() {
		Coordonnees c1= new Coordonnees(50.0, 60.0);
		Etoile e1= new Etoile(c1, 15, 10, Math.PI, 0.5);
		
		assertEquals("Remplissage Attendu", false, e1.estRempli());
	}

}

