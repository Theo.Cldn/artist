package fr.eseo.poo.projet.artiste.modele.formes;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.awt.Color;
import java.util.Locale;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;


public class LigneTest {

	@Test  //Test 4 constructeurs
	public void testContructeur1() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1,5.0,10.0);
		
		assertEquals("Abscisse Attendue",5.0,l1.getC1().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",7.0,l1.getC1().getOrdonnee(),10e-2);
		assertEquals("Largeur Attendue",5.0,l1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",10.0,l1.getHauteur(),10e-2);
	}
	
	@Test
	public void testContructeur2() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1);
		
		assertEquals("Abscisse Attendue",5.0,l1.getC1().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",7.0,l1.getC1().getOrdonnee(),10e-2);
		assertEquals("Largeur Attendue",10.0,l1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",10.0,l1.getHauteur(),10e-2);
	}
	
	@Test
	public void testContructeur3() {
		Ligne l1 = new Ligne(5.0,10.0);
		
		assertEquals("Abscisse Attendue",0.0,l1.getC1().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",0.0,l1.getC1().getOrdonnee(),10e-2);
		assertEquals("Largeur Attendue",5.0,l1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",10.0,l1.getHauteur(),10e-2);
	}
	
	@Test
	public void testContructeur4() {
		Ligne l1 = new Ligne();
		
		assertEquals("Abscisse Attendue",0.0,l1.getC1().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",0.0,l1.getC1().getOrdonnee(),10e-2);
		assertEquals("Largeur Attendue",10.0,l1.getLargeur(),10e-2);
		assertEquals("Hauteur Attendue",10.0,l1.getHauteur(),10e-2);
	}
	
	
	@Test  //Test setC1 et setC2
	public void testsetC1() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1,5.0,10.0);
		Coordonnees c2= new Coordonnees(10.0, 20.0);
		l1.setC1(c2);
		
		assertEquals("Abscisse C1 Attendue",10.0,l1.getC1().getAbscisse(),10e-2);
		assertEquals("Ordonnee C1 Attendue",20.0,l1.getC1().getOrdonnee(),10e-2);
		
	}
	
	@Test
	public void testSetC2() {
		Coordonnees c1 = new Coordonnees(5.0,5.0);
		Ligne l1 = new Ligne(c1,5.0,10.0);
		Coordonnees c2= new Coordonnees(10.0, 20.0);
		l1.setC2(c2);
		
		assertEquals("Abscisse C2 Attendue",10.0,l1.getC2().getAbscisse(),10e-2);
		assertEquals("Ordonnee C2 Attendue",20.0,l1.getC2().getOrdonnee(),10e-2);
	}
	
	
	@Test  //Test toString et changement locale
	public void testToString() {
		Coordonnees c1 = new Coordonnees(10.0, 10.0);
		Ligne l1 = new Ligne(c1,-5.0, -5.0);
		Ligne l2 = new Ligne(c1,5.0, 5.0);
		
		assertEquals("toString Attendu","[Ligne] c1 : (10,0 , 10,0) c2 : (5,0 , 5,0)"+
				" longueur : 7,07 angle : 225,0° couleur = "+l1.toStringCouleur(),l1.toString());
		assertEquals("toString Attendu","[Ligne] c1 : (10,0 , 10,0) c2 : (15,0 , 15,0)"+
				" longueur : 7,07 angle : 45,0° couleur = "+l2.toStringCouleur(),l2.toString());
		l1.setCouleur(Color.RED);
		assertEquals("toString Attendu","[Ligne] c1 : (10,0 , 10,0) c2 : (5,0 , 5,0)"+
				" longueur : 7,07 angle : 225,0° couleur = "+l1.toStringCouleur(),l1.toString());
		Locale localeEN= new Locale("EN");//test changement locale
		Locale.setDefault(localeEN);
		assertEquals("toString Attendu","[Ligne] c1 : (10.0 , 10.0) c2 : (5.0 , 5.0)"+
				" longueur : 7.07 angle : 225.0° couleur = "+l1.toStringCouleur(),l1.toString());
		Locale localeFR= new Locale("FR");  //Je remet la local de mon pc par defaut //Séurité
		Locale.setDefault(localeFR);
	}
	
	
	@Test  //Test aire
	public void testAire() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1,5.0,10.0);
		
		assertEquals("Aire Attendue",0.0,l1.aire(),10e-2);
	}
	
	
	@Test  //Test perimetre
	public void testPerimetre() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1,5.0,10.0);
			
		assertEquals("Perimetre Attendu",l1.getC1().distanceVers(l1.getC2()),l1.perimetre(),10e-2);
	}
	
	
	@Test  //Test contient
	public void testContient() {
		Coordonnees c1 = new Coordonnees(0.0,0.0);
		Coordonnees c3 = new Coordonnees(5.0,5.0009);
		Coordonnees c4 = new Coordonnees(5.0,6.0);
		Ligne l1 = new Ligne(c1,5.0,5.0);
		
		assertEquals(true, l1.contient(c3));
		assertEquals(false, l1.contient(c4));
	}
	
	
	@Test  //Test couleur
	public void testCouleur() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1,30.0,30.0);
		
		assertEquals("Couleur Attendue",Forme.COULEUR_PAR_DEFAUT,l1.getCouleur());
		l1.setCouleur(Color.RED);
		assertEquals("Couleur Attendue",Color.RED,l1.getCouleur());
	}
	
	
	//Test Forme
	@Test  //Test 4 Cadres
	public void testCadreMinX() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1,5.0,10.0);
		
		assertEquals("Abscisse Attendue",5.0,l1.getCadreMinX(),10e-2);
	}
	
	@Test
	public void testCadreMinY() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1,5.0,10.0);
		
		assertEquals("Ordonnée Attendue",7.0,l1.getCadreMinY(),10e-2);
	}
	
	@Test
	public void testCadreMaxX() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1,5.0,10.0);
		
		assertEquals("Abscisse Attendue",10.0,l1.getCadreMaxX(),10e-2);
	}
	
	@Test
	public void testCadreMaxY() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1,5.0,10.0);
		
		assertEquals("Ordonnée Attendue",17.0,l1.getCadreMaxY(),10e-2);
	}
	
	
	@Test  //Test DeplaceDe
	public void testDeplacerDe() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1,5.0,10.0);
		l1.deplacerDe(5.0, 10.0);
		
		assertEquals("Abscisse Attendue",10.0,l1.getC1().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",17.0,l1.getC1().getOrdonnee(),10e-2);
	}
	
	
	@Test  //Test DeplacerVers
	public void testDeplacerVers() {
		Coordonnees c1 = new Coordonnees(5.0,7.0);
		Ligne l1 = new Ligne(c1,5.0,10.0);
		l1.deplacerVers(5.0, 10.0);
		
		assertEquals("Abscisse Attendue",5.0,l1.getC1().getAbscisse(),10e-2);
		assertEquals("Ordonnee Attendue",10.0,l1.getC1().getOrdonnee(),10e-2);
	}
	
}
