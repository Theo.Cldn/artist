package fr.eseo.poo.projet.artiste.controleur.outils;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

public class OutilLigneTest {
	
	public  OutilLigneTest() {
		testOutilLigne();
	}
	
	private void testOutilLigne(){
		JFrame fenetre = new JFrame("Etre un Artiste");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setSize(400,240);
	
		PanneauDessin panneauDessin = new PanneauDessin();
		fenetre.setContentPane(panneauDessin);
		fenetre.pack();
		
		OutilLigne ol= new OutilLigne();
		panneauDessin.associerOutil(ol);
		
		fenetre.setVisible(true);
		fenetre.setLocationRelativeTo(null);
	}
	
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				new OutilLigneTest();
			}
		});
	}
	
}
