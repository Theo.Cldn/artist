package fr.eseo.poo.projet.artiste.controleur.outils;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

public class OutilEtoileTest {

	public  OutilEtoileTest() {
		testOutilCercle();
	}
	
	private void testOutilCercle(){
		JFrame fenetre = new JFrame("Etre un Artiste");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setSize(400,240);
		fenetre.setLayout(new BorderLayout());
	
		PanneauDessin panneauDessin = new PanneauDessin();
		PanneauBarreOutils panneauBarreOutils= new PanneauBarreOutils(panneauDessin);
		
		fenetre.add(panneauDessin, BorderLayout.CENTER);
		fenetre.add(panneauBarreOutils, BorderLayout.EAST);
		fenetre.pack();
		
		OutilEtoile oe= new OutilEtoile(panneauBarreOutils);
		panneauDessin.associerOutil(oe);
		
		fenetre.setVisible(true);
		fenetre.setLocationRelativeTo(null);
	}
	
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				new OutilEtoileTest();
			}
		});
	}

}
