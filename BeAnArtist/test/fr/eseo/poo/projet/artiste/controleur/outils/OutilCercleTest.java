package fr.eseo.poo.projet.artiste.controleur.outils;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

public class OutilCercleTest {
	
	public  OutilCercleTest() {
		testOutilCercle();
	}
	
	private void testOutilCercle(){
		JFrame fenetre = new JFrame("Etre un Artiste");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setSize(400,240);
	
		PanneauDessin panneauDessin = new PanneauDessin();
		fenetre.setContentPane(panneauDessin);
		fenetre.pack();
		
		OutilCercle oc= new OutilCercle();
		panneauDessin.associerOutil(oc);
		
		fenetre.setVisible(true);
		fenetre.setLocationRelativeTo(null);
	}
	
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				new OutilCercleTest();
			}
		});
	}

}
