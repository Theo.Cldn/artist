package fr.eseo.poo.projet.artiste.controleur.actions;

import javax.swing.SwingUtilities;


import javax.swing.JFrame;

import java.awt.BorderLayout;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;
import fr.eseo.poo.projet.artiste.controleur.outils.OutilLigne;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;


public class ActionEffacerTest {
	
	public  ActionEffacerTest() {
		testActionEffacer();
	}
	
	
	private void testActionEffacer(){
		JFrame fenetre1 = new JFrame("Etre un Artiste");
		fenetre1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre1.setLocationRelativeTo(null);
		fenetre1.setLayout(new BorderLayout());
		
		
		PanneauDessin panneauDessin = new PanneauDessin();
		
		PanneauBarreOutils panneauBarreOutils= new PanneauBarreOutils(panneauDessin);
		
		fenetre1.add(panneauDessin, BorderLayout.CENTER);
		fenetre1.add(panneauBarreOutils, BorderLayout.EAST);
		
		OutilLigne ol= new OutilLigne();
		panneauDessin.associerOutil(ol);
		
		
		fenetre1.setSize(700,400);
		fenetre1.setVisible(true);
	}
	
	
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				new ActionEffacerTest();
			}
		});
	}

}
