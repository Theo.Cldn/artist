package fr.eseo.poo.projet.artiste.vue.ihm;

import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.ButtonGroup;

import java.awt.Dimension;

import fr.eseo.poo.projet.artiste.controleur.actions.ActionChoisirCouleur;
import fr.eseo.poo.projet.artiste.controleur.actions.ActionChoisirForme;
import fr.eseo.poo.projet.artiste.controleur.actions.ActionChoisirRemplissage;
import fr.eseo.poo.projet.artiste.controleur.actions.ActionEffacer;
import fr.eseo.poo.projet.artiste.controleur.actions.ActionExport;
import fr.eseo.poo.projet.artiste.controleur.actions.ActionSelectionner;
import fr.eseo.poo.projet.artiste.controleur.actions.ActionImport;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;


public class PanneauBarreOutils extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String BRANCHE_SPINNER_NOM = "Nombre branches";
	public static final String LONGUEUR_SPINNER_NOM = "Longueur branches";
	
	private static SpinnerNumberModel modelNombreBranches;
	private static SpinnerNumberModel modelLongueurBranches;
	private PanneauDessin panneauDessin;
	

	public PanneauBarreOutils(PanneauDessin panneauDessin) { //Constructor
		this.panneauDessin= panneauDessin;
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); //BoxLayout following Y
		initComponents(); //init components
	}
	
	public void initComponents() {
		
		//Erase action
		//Add button to Jpanel
		ActionEffacer actionEffacer= new ActionEffacer(panneauDessin);
		JButton effacerTout= new JButton(actionEffacer);
		effacerTout.setName(ActionEffacer.NOM_ACTION);
		this.add(effacerTout);
		
		
		
		this.add(Box.createRigidArea(new Dimension(0,20)));
		
		
		
		//Group buttons into 1 box cause 1 tool at time (JToggle car ON, OFF)
		ButtonGroup buttonGroup= new ButtonGroup();			//Group of buttons
		
		
		//Creation of actions
		//Add buttons to Jpanel
		//Add buttons to ButtonGroup
		ActionChoisirForme actionChoisirLigne= new ActionChoisirForme(panneauDessin,this,"Ligne");
		JToggleButton ligne= new JToggleButton(actionChoisirLigne);
		ligne.setName(ActionChoisirForme.NOM_ACTION_LIGNE);
		this.add(ligne);
		buttonGroup.add(ligne);
		
		ActionChoisirForme actionChoisirEllipse= new ActionChoisirForme(panneauDessin,this,"Ellipse");
		JToggleButton ellipse= new JToggleButton(actionChoisirEllipse);
		ellipse.setName(ActionChoisirForme.NOM_ACTION_ELLIPSE);
		this.add(ellipse);
		buttonGroup.add(ellipse);
		
		ActionChoisirForme actionChoisirCercle= new ActionChoisirForme(panneauDessin,this,"Cercle");
		JToggleButton cercle= new JToggleButton(actionChoisirCercle);
		cercle.setName(ActionChoisirForme.NOM_ACTION_CERCLE);
		this.add(cercle);
		buttonGroup.add(cercle);
		
		ActionChoisirForme actionChoisirEtoile= new ActionChoisirForme(panneauDessin,this,"Etoile");
		JToggleButton etoile= new JToggleButton(actionChoisirEtoile);
		etoile.setName(ActionChoisirForme.NOM_ACTION_ETOILE);
		this.add(etoile);
		buttonGroup.add(etoile);
		
		
		this.add(Box.createRigidArea(new Dimension(0,5)));
		
		
		ActionSelectionner actionSelectionner= new ActionSelectionner(panneauDessin);
		JToggleButton selectionner= new JToggleButton(actionSelectionner);
		selectionner.setName(ActionSelectionner.NOM_ACTION);
		this.add(selectionner);
		buttonGroup.add(selectionner);
		
		
		
		this.add(Box.createRigidArea(new Dimension(0,20)));
		
		ActionChoisirCouleur actionChoisirCouleur= new ActionChoisirCouleur(panneauDessin);
		JButton couleur= new JButton(actionChoisirCouleur);
		couleur.setName(ActionChoisirCouleur.NOM_ACTION);
		this.add(couleur);
		
		this.add(Box.createRigidArea(new Dimension(0,10)));
		
		ActionChoisirRemplissage actionChoisirRemplissage= new ActionChoisirRemplissage(panneauDessin);
		JCheckBox jCheckBox= new JCheckBox(actionChoisirRemplissage);
		jCheckBox.setName(ActionChoisirRemplissage.NOM_ACTION);
		this.add(jCheckBox);
		
		this.add(Box.createRigidArea(new Dimension(0,10)));
		
		JLabel JLabelNbBranches = new JLabel(BRANCHE_SPINNER_NOM);
		modelNombreBranches = new SpinnerNumberModel(Etoile.NOMBRE_BRANCHES_PAR_DEFAUT,3,15,1);
		JSpinner nombreBranches = new JSpinner(modelNombreBranches);
		nombreBranches.setName(BRANCHE_SPINNER_NOM);
		this.add(JLabelNbBranches);
		this.add(nombreBranches);
				
		
		JLabel JLabelLgBranches = new JLabel(LONGUEUR_SPINNER_NOM);
		modelLongueurBranches = new SpinnerNumberModel(Etoile.LONGUEUR_BRANCHE_PAR_DEFAUT,0,1,0.01);
		JSpinner longueurBranches = new JSpinner(modelLongueurBranches);
		longueurBranches.setName(LONGUEUR_SPINNER_NOM);
		this.add(JLabelLgBranches);
		this.add(longueurBranches);
		
		
		
		ActionImport actionImport= new ActionImport(panneauDessin);
		JButton importDessin= new JButton(actionImport);
		importDessin.setName(ActionImport.NOM_ACTION);
		this.add(importDessin);
		
		ActionExport actionExport= new ActionExport(panneauDessin);
		JButton export= new JButton(actionExport);
		export.setName(ActionExport.NOM_ACTION);
		this.add(export);
	}
	
	
	public int getNbBranches() {
		return (int) modelNombreBranches.getValue();
	}
	
	public double getLongueurBranche() {
		return (double) modelLongueurBranches.getValue();
	}

}
