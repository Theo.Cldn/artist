package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.Graphics2D;

import fr.eseo.poo.projet.artiste.modele.formes.Forme;


public abstract class VueForme { //Abstract Class cause VueForme depends on Forms
	
	protected final Forme forme;

	public VueForme(Forme forme) { //Constructor
		this.forme=forme;
	}
	
	public Forme getForme() {
		return forme;
	}
	
	public abstract void affiche(Graphics2D g2d); //Abstract Method cause display VueForme that depends on Form
	
}	
