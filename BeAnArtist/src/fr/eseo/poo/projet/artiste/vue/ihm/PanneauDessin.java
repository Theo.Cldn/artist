package fr.eseo.poo.projet.artiste.vue.ihm;

import javax.swing.JPanel;

import java.awt.Dimension;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import java.util.List;
import java.util.ArrayList;

import fr.eseo.poo.projet.artiste.controleur.outils.Outil;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;


public class PanneauDessin extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int LARGEUR_PAR_DEFAUT= 700;
	public static final int HAUTEUR_PAR_DEFAUT= 400;
	public static final Color COULEUR_FOND_PAR_DEFAUT= Color.WHITE;
	public static final boolean MODE_REMPLISSAGE= false;
	
	private final List<VueForme> vueFormes = new ArrayList<VueForme>(); //Creation of VueForme instances list
	private Outil outilCourant; //Current tools
	private Color couleurCourante; //Current color
	private boolean modeRemplissage; //Filling mode
	
	
	//Constructors
	public PanneauDessin() {
		this(LARGEUR_PAR_DEFAUT,HAUTEUR_PAR_DEFAUT,COULEUR_FOND_PAR_DEFAUT);
	}
	
	public PanneauDessin(int largeur, int hauteur, Color color) {
		this.setPreferredSize(new Dimension(largeur,hauteur));
		this.setBackground(color);
		this.couleurCourante= Forme.COULEUR_PAR_DEFAUT;
		this.modeRemplissage= MODE_REMPLISSAGE;
	}
	
	
	//Accessors and mutators
	public List<VueForme> getVueFormes() {
		return this.vueFormes;
	}
	
	public Outil getOutilCourant() {
		return outilCourant;
	}
	
	private void setOutilCourant(Outil outilCourant) {
		this.outilCourant=outilCourant;
	}
	
	public Color getCouleurCourante() {
		return this.couleurCourante;
	}
	
	public void setCouleurCourante(Color couleur) {
		this.couleurCourante= couleur;
	}
	
	public boolean getModeRemplissage() {
		return this.modeRemplissage;
	}
	
	public void setModeRemplissage(boolean modeRemplissage) {
		this.modeRemplissage= modeRemplissage;
	}
	
	
	//Add VueForme instances to VueForme list
	public void ajouterVueForme(VueForme vueForme) {
		this.vueFormes.add(vueForme);
	}
	
	//Method to paint vueForme instances in the VueForme list
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2D = (Graphics2D)g.create();
		for(int i=0;i<this.getVueFormes().size();i++) {
			g2D.setColor(getCouleurCourante());
			getVueFormes().get(i).affiche(g2D);
		}
		this.repaint();  //repaint
		g2D.dispose();  //free
	}

	
	//Associate a tool to the drawing board
	public void associerOutil(Outil outil) {
		if(outil!=null) {
			if(getOutilCourant()!=null) {
				dissocierOutil();
				setOutilCourant(outil);
				getOutilCourant().setPanneauDessin(this);
				this.addMouseListener(outilCourant);
				this.addMouseMotionListener(outilCourant);
			}
			else {
				setOutilCourant(outil);
				getOutilCourant().setPanneauDessin(this);
				this.addMouseListener(outilCourant);
				this.addMouseMotionListener(outilCourant);
			}
		}
	}
	
	//Dissociate a tool used by the drawing board
	//Method called by the previous method so listeners will be added in the previous method
	private void dissocierOutil() {
		this.removeMouseListener(getOutilCourant());
		this.removeMouseMotionListener(getOutilCourant());
		getOutilCourant().setPanneauDessin(null);
		setOutilCourant(null);
	}
}
