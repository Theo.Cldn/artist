package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.Color;
import java.awt.Graphics2D;

import fr.eseo.poo.projet.artiste.modele.formes.Etoile;


public class VueEtoile extends VueForme{
	
	public VueEtoile(Etoile etoile) {
		super(etoile);
	}
	
	public void affiche(Graphics2D g2d) {
		Color ref = g2d.getColor();

		int[] sommetAbscisse = new int[((Etoile)this.getForme()).getNombreBranches()*2];
		int[] sommetOrdonnee = new int[((Etoile)this.getForme()).getNombreBranches()*2];
		for (int i = 0 ; i < ((Etoile)this.getForme()).getNombreBranches()*2 ; i++) {
			sommetAbscisse[i]=(int)Math.round(((Etoile)this.getForme()).getCoordonnees().get(i).getAbscisse());
			sommetOrdonnee[i]=(int)Math.round(((Etoile)this.getForme()).getCoordonnees().get(i).getOrdonnee());
		}
		
		g2d.setColor(this.getForme().getCouleur());
		Etoile etoile= (Etoile) getForme();
		g2d.drawPolygon(sommetAbscisse, sommetOrdonnee, sommetOrdonnee.length);
		if (etoile.estRempli()) {
			g2d.fillPolygon(sommetAbscisse, sommetOrdonnee, sommetOrdonnee.length);
		}
		g2d.setColor(ref);
	}

}
