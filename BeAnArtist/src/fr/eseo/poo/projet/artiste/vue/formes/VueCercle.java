package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.Color;
import java.awt.Graphics2D;

import fr.eseo.poo.projet.artiste.modele.formes.Cercle;


public class VueCercle extends VueEllipse{
	
	public VueCercle(Cercle cercle) {
		super(cercle);
	}
	
	public void affiche(Graphics2D g2d) {
		Color ref= g2d.getColor();
		int x= (int) Math.round(((Cercle) this.getForme()).getPosition().getAbscisse());
		int y= (int) Math.round(((Cercle) this.getForme()).getPosition().getOrdonnee());
		int largeur= (int) Math.round(((Cercle) this.getForme()).getLargeur());
		int hauteur= (int) Math.round(((Cercle) this.getForme()).getHauteur());
		
		g2d.setColor(getForme().getCouleur());
		Cercle cercle= (Cercle) getForme();
		g2d.drawOval(x, y, largeur, hauteur);
		if(cercle.estRempli()) {
			g2d.fillOval(x, y, largeur, hauteur);
		}
		g2d.setColor(ref);
	}
	

}
