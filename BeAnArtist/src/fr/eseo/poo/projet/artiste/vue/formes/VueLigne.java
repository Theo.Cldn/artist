package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.Color;
import java.awt.Graphics2D;

import fr.eseo.poo.projet.artiste.modele.formes.Ligne;


public class VueLigne extends VueForme{

	public VueLigne(Ligne ligne) {
		super(ligne);
	}
	
	public void affiche(Graphics2D g2d) {
		Color ref= g2d.getColor();
		int x1= (int) Math.round(((Ligne) this.getForme()).getC1().getAbscisse());
		int y1= (int) Math.round(((Ligne) this.getForme()).getC1().getOrdonnee());
		int x2= (int) Math.round(((Ligne) this.getForme()).getC2().getAbscisse());
		int y2= (int) Math.round(((Ligne) this.getForme()).getC2().getOrdonnee());
		g2d.setColor(getForme().getCouleur());
		g2d.drawLine(x1,y1,x2,y2);
		g2d.setColor(ref);
	}
	
}
