package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.Color;
import java.awt.Graphics2D;

import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;


public class VueEllipse extends VueForme{
	
	public VueEllipse(Ellipse ellipse) {
		super(ellipse);
	}
	
	public void affiche(Graphics2D g2d) {
		Color ref= g2d.getColor();
		int x= (int) Math.round(((Ellipse) this.getForme()).getPosition().getAbscisse());
		int y= (int) Math.round(((Ellipse) this.getForme()).getPosition().getOrdonnee());
		int largeur= (int) Math.round(((Ellipse) this.getForme()).getLargeur());
		int hauteur= (int) Math.round(((Ellipse) this.getForme()).getHauteur());
		
		g2d.setColor(getForme().getCouleur());
		Ellipse ellipse= (Ellipse) getForme();
		g2d.drawOval(x, y, largeur, hauteur);
		if(ellipse.estRempli()) {
			g2d.fillOval(x, y, largeur, hauteur);
		}
		g2d.setColor(ref);
	}

}
