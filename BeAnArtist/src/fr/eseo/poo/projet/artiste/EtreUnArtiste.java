package fr.eseo.poo.projet.artiste;

import java.awt.BorderLayout;


import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

public class EtreUnArtiste{
	
	public static void main(String[] args) {
		
		JFrame beAnArtist= new JFrame();
		
		beAnArtist.setTitle("Be an Artist");
		beAnArtist.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		beAnArtist.setLocationRelativeTo(null);
		beAnArtist.setLayout(new BorderLayout());
		
		PanneauDessin panneauDessin = new PanneauDessin();
		PanneauBarreOutils panneauBarreOutils= new PanneauBarreOutils(panneauDessin);
		
		beAnArtist.add(panneauDessin, BorderLayout.CENTER);
		beAnArtist.add(panneauBarreOutils, BorderLayout.EAST);
		
		beAnArtist.setSize(700,400);
		beAnArtist.setVisible(true);
	}

}
