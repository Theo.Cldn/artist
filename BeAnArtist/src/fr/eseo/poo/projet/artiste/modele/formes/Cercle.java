package fr.eseo.poo.projet.artiste.modele.formes;

import java.text.DecimalFormat;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;

//Circle is an particular ellipse where height and width are similar
public class Cercle extends Ellipse{
	
	
	//Constructors
	public Cercle() {
		this(new Coordonnees(), LARGEUR_PAR_DEFAUT);
	}
	
	public Cercle(double taille) {
		this(new Coordonnees(), taille);
	}
	
	public Cercle(Coordonnees coordonnees) {
		this(coordonnees, LARGEUR_PAR_DEFAUT);
	}
	
	public Cercle(Coordonnees coordonnees, double taille) {
		super(coordonnees, taille, taille);
	}
	
	
	//Accessors and mutators
	public void setLargeur(double largeur) {
		if(largeur<0) {
			throw new IllegalArgumentException("largeur et hauteur doivent être positives");
		}
		this.largeur=largeur;
		this.hauteur=largeur;
	}
	
	public void setHauteur(double hauteur) {
		if(hauteur<0) {
			throw new IllegalArgumentException("largeur et hauteur doivent être positives");
		}
		this.hauteur=hauteur;
		this.largeur=hauteur;
	}
	
	
	//String
	public String toString() {
		DecimalFormat df =new DecimalFormat("0.0#");
		String position= getPosition().toString();
		String largeur= df.format(getLargeur());
		String hauteur= df.format(getHauteur());
		String perimetre= df.format(perimetre());
		String aire= df.format(aire());
		String couleur= toStringCouleur();
		String rempli= (estRempli())? rempli= "-Rempli" : "";
		return "[Cercle"+rempli+"]"+" : pos "+position+" dim "+largeur+" x "+hauteur+
				" périmètre : "+perimetre+" aire : "+aire+" couleur = "+couleur;
	}
	
	//Perimeter of the shape (could be herited from ellipse class like area method)
	public double perimetre() {
		double perimetre= 2*Math.PI*(getLargeur()/2);
		return perimetre;
	}
	
	//Contain a point
	public boolean contient(Coordonnees coordonnees) {
		double centreX= getPosition().getAbscisse()+getLargeur()/2;
		double centreY= getPosition().getOrdonnee()+getHauteur()/2;
		Coordonnees centre = new Coordonnees(centreX, centreY);
		if(centre.distanceVers(coordonnees) <= getLargeur()/2+EPSILON) {
			return true;
		}
		else {
			return false;
		}
	}
	
}
