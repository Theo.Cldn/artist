package fr.eseo.poo.projet.artiste.modele.formes;

import java.text.DecimalFormat;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;


public class Ligne extends Forme{
	
	public static final double EPSILON= 10e-3;
	

	//Constructors
	public Ligne(Coordonnees coordonnees, double largeur, double hauteur) {
		super(coordonnees,largeur,hauteur);
	}
	
	public Ligne(Coordonnees coordonnees) {
		super(coordonnees);
	}
	
	public Ligne(double largeur, double hauteur) {
		super(largeur,hauteur);
	}
	
	public Ligne() {
		super();
	}
	
	
	//Accessors and mutators
	public Coordonnees getC1() {
		return getPosition();
	}
	
	public Coordonnees getC2() {
		Coordonnees c2 = new Coordonnees(getC1().getAbscisse()+this.getLargeur(),getC1().getOrdonnee()+this.getHauteur());
		return c2;
	}
	
	
	public void setC1(Coordonnees c1) {
		this.setLargeur(getC2().getAbscisse()-c1.getAbscisse());
		this.setHauteur(getC2().getOrdonnee()-c1.getOrdonnee());
		this.setPosition(c1);
	}
	
	public void setC2(Coordonnees c2) {
		this.setLargeur(c2.getAbscisse()-getC1().getAbscisse());
		this.setHauteur(c2.getOrdonnee()-getC1().getOrdonnee());
	}
	
	
	//String
	public String toString() {
		DecimalFormat df =new DecimalFormat("0.0#");
		double perimetre = perimetre();
		double angle = getC1().angleVers(getC2())*(180/Math.PI);
		String couleur= toStringCouleur();
		if(angle<0) {angle=360+angle;}
		return "[Ligne]"+" c1 : "+getC1().toString()+" c2 : "+getC2().toString()+" longueur : "
		+df.format(perimetre)+" angle : "+df.format(angle)+"°"+" couleur = "+couleur;
	}
	
	
	//Area of the shape
	public double aire() {
		return 0.0;
	}
	
	//Perimeter of the shape
	public double perimetre() {
		double a= Math.pow(getC2().getAbscisse()-getC1().getAbscisse(), 2);
		double b= Math.pow(getC2().getOrdonnee()-getC1().getOrdonnee(), 2);
		return Math.sqrt(a+b);
	}
	
	//Contain a point
	public boolean contient(Coordonnees coordonnees) {
		double chemin= Math.abs(getC1().distanceVers(coordonnees))+Math.abs(coordonnees.distanceVers(getC2()));
		double cheminDirect= getC1().distanceVers(getC2());
		if(chemin - cheminDirect <= EPSILON) {
			return true;
		}
		else {
			return false;
		}
	}
	
}
