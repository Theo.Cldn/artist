package fr.eseo.poo.projet.artiste.modele.formes;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.Remplissable;


public class Etoile extends Forme implements Remplissable {
	
	private static final boolean MODE_REMPLISSAGE= false;
	public static final double TAILLE_PAR_DEFAUT= Forme.LARGEUR_PAR_DEFAUT;
	public static final int NOMBRE_BRANCHES_PAR_DEFAUT= 9;
	public static final double ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT= 0;
	public static final double LONGUEUR_BRANCHE_PAR_DEFAUT= 0.5;
	
	private List<Coordonnees> coordonnees= new ArrayList<Coordonnees>();
	
	private boolean modeRemplissage;
	private int nbBranches;
	private double anglePremBranche;
	private double longueurBranche;
	
	
	//Constructors
	public Etoile() {
		this(new Coordonnees(), TAILLE_PAR_DEFAUT, NOMBRE_BRANCHES_PAR_DEFAUT, 
				ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT, LONGUEUR_BRANCHE_PAR_DEFAUT);
	}
	
	public Etoile(double taille) {
		this(new Coordonnees(), taille, NOMBRE_BRANCHES_PAR_DEFAUT, 
				ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT, LONGUEUR_BRANCHE_PAR_DEFAUT);
	}
	
	public Etoile(Coordonnees coordonnees) {
		this(coordonnees, TAILLE_PAR_DEFAUT, NOMBRE_BRANCHES_PAR_DEFAUT, 
				ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT, LONGUEUR_BRANCHE_PAR_DEFAUT);
	}
	
	public Etoile(Coordonnees coordonnees, double taille) {
		this(coordonnees, taille, NOMBRE_BRANCHES_PAR_DEFAUT, 
				ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT, LONGUEUR_BRANCHE_PAR_DEFAUT);
	}
	
	public Etoile(Coordonnees coordonnees,double taille,int nbBranches,double anglePremBranche,double longueurBranche) {
		super(coordonnees, taille, taille);
		
		if(taille<0) {
			throw new IllegalArgumentException("Respectez les valeurs");
		}
		if(nbBranches<3 || nbBranches>15) {
			throw new IllegalArgumentException("Respectez les valeurs");
		}
		if(anglePremBranche<-Math.PI || anglePremBranche>Math.PI) {
			throw new IllegalArgumentException("Respectez les valeurs");
		}
		if(longueurBranche<0 || longueurBranche>1) {
			throw new IllegalArgumentException("Respectez les valeurs");
		}
		this.anglePremBranche= anglePremBranche;
		this.nbBranches= nbBranches;
		this.longueurBranche= longueurBranche;
		this.modeRemplissage= MODE_REMPLISSAGE;
		
		this.calculSommetEtoile();
	}
	
	
	public void calculSommetEtoile() {
		double centreX= getPosition().getAbscisse()+getLargeur()/2;
		double centreY= getPosition().getOrdonnee()+getLargeur()/2;
		Coordonnees centre = new Coordonnees(centreX,centreY);
		double angleBranche = (2*Math.PI)/this.getNombreBranches();
		double angleActuel = this.getAnglePremiereBranche();
		for(int i=0;i<this.getNombreBranches();i++) {
			getCoordonnees().add(new Coordonnees(centre.getAbscisse()+(this.getLargeur()/2)
					*Math.cos(angleActuel), centre.getOrdonnee()+(this.getLargeur()/2)*Math.sin(angleActuel)));
			getCoordonnees().add(new Coordonnees(centre.getAbscisse()+(this.getLargeur()/2)
					*Math.cos(angleActuel+angleBranche/2)*(1-longueurBranche),centre.getOrdonnee()
					+(this.getLargeur()/2)*Math.sin(angleActuel+angleBranche/2)*(1-longueurBranche)));
			angleActuel += angleBranche;
		}
	}
	
	//private void calculSommetEtoile2() { //test
		//double centreX= getPosition().getAbscisse();
		//double centreY= getPosition().getOrdonnee();
		//double rayon= getLargeur()/2;
		//double longueur= getLongueurBranche()*rayon;
		//for(double alpha=anglePremBranche; alpha>anglePremBranche-2*Math.PI;alpha=alpha-(2*Math.PI/getNombreBranches())) {
			//double xg= centreX+rayon*Math.cos(alpha);
			//double yg= centreY-rayon*Math.sin(alpha);
			//double xp= centreX+(rayon-longueur)*Math.cos(alpha-Math.PI/getNombreBranches());
			//double yp= centreY-(rayon-longueur)*Math.sin(alpha-Math.PI/getNombreBranches());
			//Coordonnees coGBranches= new Coordonnees(xg,yg);
			//Coordonnees coPBranches= new Coordonnees(xp,yp);
			//getCoordonnees().add(coGBranches);
			//getCoordonnees().add(coPBranches);
		//}
	//}
	
	//Accessors and mutators
	public void setLargeur(double largeur) {
		this.largeur= largeur;
		this.hauteur= largeur;
		if(largeur<0) {
			throw new IllegalArgumentException("Respectez les valeurs");
		}
		getCoordonnees().clear();
		this.calculSommetEtoile();
	}
	
	public void setHauteur(double hauteur) {
		this.hauteur= hauteur;
		this.largeur= hauteur;
		if(hauteur<0) {
			throw new IllegalArgumentException("Respectez les valeurs");
		}
		getCoordonnees().clear();
		this.calculSommetEtoile();
	}
	
	public void setNombreBranches(int nBranches) {
		this.nbBranches= nBranches;
		if(nbBranches<3 || nbBranches>15) {
			throw new IllegalArgumentException("Respectez les valeurs");
		}
		getCoordonnees().clear();
		this.calculSommetEtoile();
	}
	
	public void setAnglePremiereBranche(double anglePreBranche) {
		this.anglePremBranche=  anglePreBranche;
		if(anglePremBranche<-Math.PI || anglePremBranche>Math.PI) {
			throw new IllegalArgumentException("Respectez les valeurs");
		}
		getCoordonnees().clear();
		this.calculSommetEtoile();
	}
	
	public void setLongueurBranche(double longueurBranche) {
		this.longueurBranche= longueurBranche;
		if(longueurBranche<0 || longueurBranche>1) {
			throw new IllegalArgumentException("Respectez les valeurs");
		}
		getCoordonnees().clear();
		this.calculSommetEtoile();
	}
	
	
	public List<Coordonnees> getCoordonnees() {
		return this.coordonnees;
	}

	public int getNombreBranches() {
		return this.nbBranches;
	}
	
	public double getAnglePremiereBranche() {
		return this.anglePremBranche;
	}
	
	public double getLongueurBranche() {
		return this.longueurBranche;
	}
	
	public void setRempli(boolean modeRemplissage) {
		this.modeRemplissage=modeRemplissage;
	}
	
	public boolean estRempli() {
		return this.modeRemplissage;
	}

	
	//String
	public String toString() {
		DecimalFormat df =new DecimalFormat("0.0#");
		String position= getPosition().toString();
		String largeur= df.format(getLargeur());
		String hauteur= df.format(getHauteur());
		String perimetre= df.format(perimetre());
		String aire= df.format(aire());
		String couleur= toStringCouleur();
		String rempli= (estRempli())? rempli= "-Rempli" : "";
		return "[Etoile"+rempli+"]"+" : pos "+position+" dim "+largeur+" x "+hauteur+
				" périmètre : "+perimetre+" aire : "+aire+" couleur = "+couleur;
	}
	
	
	//Area of the shape
	public double aire() {
		double largeurCerfVolant = this.getCoordonnees().get(1).distanceVers(this.getCoordonnees().get(3));
		double longueurCerfVolant = this.getHauteur()/2;
		return this.getNombreBranches()*largeurCerfVolant*longueurCerfVolant/2;
	}
	
	
	//Perimeter of the shape
	public double perimetre() {
		return coordonnees.get(0).distanceVers(coordonnees.get(1))*2*getNombreBranches();
	}
	
	
	//Contain a point
	public boolean contient(Coordonnees coordonnees) { // Pris chez Thomas Jeannette car GPI1 non fini
		for (int i=0 ; i<this.getNombreBranches() ; i++) {
			Coordonnees point1 = this.coordonnees.get(2*i);
			Coordonnees point2 = this.coordonnees.get(2*i+1);
			Coordonnees point3 = new Coordonnees(this.getPosition().getAbscisse()+this.getLargeur()/2
					,this.getPosition().getOrdonnee()+getLargeur()/2);
			Coordonnees point4;
			if (i==0) {
				point4 = this.coordonnees.get(this.getNombreBranches()*2+2*i-1);
			}
			else {
				point4 = this.coordonnees.get(2*i-1);
			}
			double signe1 = (coordonnees.getAbscisse()-point2.getAbscisse())*(point1.getOrdonnee()-point2.getOrdonnee())
					-(point1.getAbscisse()-point2.getAbscisse())*(coordonnees.getOrdonnee()-point2.getOrdonnee());
			double signe2 = (coordonnees.getAbscisse()-point3.getAbscisse())*(point2.getOrdonnee()-point3.getOrdonnee())
					-(point2.getAbscisse()-point3.getAbscisse())*(coordonnees.getOrdonnee()-point3.getOrdonnee());
			double signe3 = (coordonnees.getAbscisse()-point4.getAbscisse())*(point3.getOrdonnee()-point4.getOrdonnee())
					-(point3.getAbscisse()-point4.getAbscisse())*(coordonnees.getOrdonnee()-point4.getOrdonnee());
			double signe4 = (coordonnees.getAbscisse()-point1.getAbscisse())*(point4.getOrdonnee()-point1.getOrdonnee())
					-(point4.getAbscisse()-point1.getAbscisse())*(coordonnees.getOrdonnee()-point1.getOrdonnee());
			if ((signe1 >= -EPSILON && signe2 >= -EPSILON && signe3 >= -EPSILON && signe4 >= -EPSILON) 
					|| (signe1 <= EPSILON && signe2 <= EPSILON &&  signe3 <= EPSILON && signe4 <= EPSILON)) {
				return true;
			}
		}
		return false;
	}
	
}
