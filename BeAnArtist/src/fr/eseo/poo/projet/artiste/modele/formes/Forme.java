package fr.eseo.poo.projet.artiste.modele.formes;

import java.awt.Color;
import java.util.Locale;

import javax.swing.UIManager;

import fr.eseo.poo.projet.artiste.modele.Coloriable;
import fr.eseo.poo.projet.artiste.modele.Coordonnees;

//All shapes are coloriables so the Shape Class (Forme) implements Coloriable but not Filling Class (Remplissage) due to Line Class (Ligne)
public abstract class Forme implements Coloriable{
	
	//Default values
	public static final double EPSILON= 10e-3;
	public static final double LARGEUR_PAR_DEFAUT=10.0;
	public static final double HAUTEUR_PAR_DEFAUT=10.0;
	public static final Color COULEUR_PAR_DEFAUT= UIManager.getColor("Panel.foreground");
	
	protected double largeur;
	protected double hauteur;
	private Coordonnees position;
	protected Color couleur;
	
	
	//Constructors
	public Forme(Coordonnees position, double largeur, double hauteur) {
		this.position=position;
		this.largeur=largeur;
		this.hauteur=hauteur;
		this.couleur= COULEUR_PAR_DEFAUT;
	}
	
	public Forme(Coordonnees position) {
		this(position,LARGEUR_PAR_DEFAUT,HAUTEUR_PAR_DEFAUT);
	}
	
	public Forme(double largeur, double hauteur) {
		this(new Coordonnees(),largeur,hauteur);
	}
	
	public Forme() {
		this(new Coordonnees(),LARGEUR_PAR_DEFAUT,HAUTEUR_PAR_DEFAUT);
	}
	
	
	//Accessors and mutators
	public void setPosition(Coordonnees position) {
		this.position=position;
	}
	
	public void setLargeur(double largeur) {
		this.largeur=largeur;
	}
	
	public void setHauteur(double hauteur) {
		this.hauteur=hauteur;
	}
	
	public Coordonnees getPosition() {
		return position;
	}

	public double getLargeur() {
		return largeur;
	}
	
	public double getHauteur() {
		return hauteur;
	}
	
	public double getCadreMinX() {
		return position.getAbscisse()+Math.min(largeur, 0);	
	}
	
	public double getCadreMinY() {
		return position.getOrdonnee()+Math.min(hauteur, 0);
	}
	
	public double getCadreMaxX() {
		return position.getAbscisse()+Math.max(largeur, 0);
	}
	
	public double getCadreMaxY() {
		return position.getOrdonnee()+Math.max(hauteur, 0);
	}
	
	public void setCouleur(Color couleur) {
		this.couleur= couleur;
	}
	
	public Color getCouleur() {
		return this.couleur;
	}
	
	
	//Move shape from delta
	public void deplacerDe(double deltaX, double deltaY) {
		this.position.deplacerDe(deltaX,deltaY);
	}
	
	//Move shape to new position
	public void deplacerVers(double nouvelleAbscisse, double nouvelleOrdonnee) {
		this.position.deplacerVers(nouvelleAbscisse,nouvelleOrdonnee);
	}
	
	
	//Abstract method because depends on the shape
	public abstract double aire();
	public abstract double perimetre();
	public abstract boolean contient(Coordonnees coordonnees);
	

	//String
	public String toStringCouleur() {
		int r= getCouleur().getRed();
		int v= getCouleur().getGreen();
		int b= getCouleur().getBlue();
		Locale fr= new Locale("FR");
		if(Locale.getDefault().getLanguage()== fr.getLanguage()) {
			return "R"+r+","+"V"+v+","+"B"+b;
		}
		else {
			return "R"+r+","+"G"+v+","+"B"+b;
		}
	}
	
}
