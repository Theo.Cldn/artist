package fr.eseo.poo.projet.artiste.modele.formes;

import java.text.DecimalFormat;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.Remplissable;


public class Ellipse extends Forme implements Remplissable{
	
	private static final boolean MODE_REMPLISSAGE= false;
	private boolean modeRemplissage;
	
	
	//Constructors
	public Ellipse() {
		this(new Coordonnees(), LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT);
	}
	
	public Ellipse(double largeur, double hauteur) {
		this(new Coordonnees(), largeur, hauteur);
	}
	
	public Ellipse(Coordonnees coordonnees) {
		this(coordonnees, LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT);
	}
	
	public Ellipse(Coordonnees coordonnees, double largeur, double hauteur) {
		super(coordonnees, largeur, hauteur);
		this.modeRemplissage= MODE_REMPLISSAGE;
		if(largeur<0 || hauteur<0) {
			throw new IllegalArgumentException("largeur et hauteur doivent être positives");
		}
	}
	
	
	//Accessors and mutators
	public void setHauteur(double hauteur) {
		if(hauteur<0) {
			throw new IllegalArgumentException("largeur et hauteur doivent être positives");
		}
		this.hauteur=hauteur;
	}
	
	public void setLargeur(double largeur) {
		if(largeur<0) {
			throw new IllegalArgumentException("largeur et hauteur doivent être positives");
		}
		this.largeur=largeur;
	}
	
	public void setRempli(boolean modeRemplissage) {
		this.modeRemplissage=modeRemplissage;
	}
	
	public boolean estRempli() {
		return this.modeRemplissage;
	}
	
	
	//String
	public String toString() {
		DecimalFormat df =new DecimalFormat("0.0#");
		String position= getPosition().toString();
		String largeur= df.format(getLargeur());
		String hauteur= df.format(getHauteur());
		String perimetre= df.format(perimetre());
		String aire= df.format(aire());
		String couleur= toStringCouleur();
		String rempli= (estRempli())? rempli= "-Rempli" : "";
		return "[Ellipse"+rempli+"]"+" : pos "+position+" dim "+largeur+" x "+hauteur+
				" périmètre : "+perimetre+" aire : "+aire+" couleur = "+couleur;
	}
	
	
	//Area of the shape
	public double aire() {
		return Math.PI*(getLargeur()*getHauteur())/4;
	}
	
	
	//Perimeter of the shape
	public double perimetre() {
		double a= getLargeur()/2;
		double b= getHauteur()/2;
		double h= Math.pow((Math.abs(a-b))/(a+b), 2);
		return Math.PI*(a+b)*(1+(3*h/(10+Math.sqrt(4-3*h))));
	}
	
	
	//Contain a point
	public boolean contient(Coordonnees coordonnees) {
		double centreX= getPosition().getAbscisse()+getLargeur()/2;
		double centreY= getPosition().getOrdonnee()+getHauteur()/2;
		Coordonnees centre = new Coordonnees(centreX, centreY);
		double a=getLargeur()/2;
		double b=getHauteur()/2;
		double angle= centre.angleVers(coordonnees);
		Coordonnees pR= new Coordonnees(centreX+a*Math.cos(angle),centreY+b*Math.sin(angle)); 
		if(centre.distanceVers(coordonnees) <= centre.distanceVers(pR)+EPSILON) {
			return true;
		}
		else {
			return false;
		}
	}
	
}
