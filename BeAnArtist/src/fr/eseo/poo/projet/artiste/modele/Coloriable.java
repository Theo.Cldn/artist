package fr.eseo.poo.projet.artiste.modele;

import java.awt.Color;

//Interface for coloring shapes
public interface Coloriable {
	
	public abstract Color getCouleur();
	
	public void setCouleur(Color couleur);

}
