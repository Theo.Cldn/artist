package fr.eseo.poo.projet.artiste.modele;

//Interface for filling shapes
public interface Remplissable {
	
	public abstract boolean estRempli();
	
	public void setRempli(boolean modeRemplissage);

}
