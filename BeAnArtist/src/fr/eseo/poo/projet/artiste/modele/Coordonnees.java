package fr.eseo.poo.projet.artiste.modele;

import java.text.DecimalFormat;


public class Coordonnees {
	
	public static final double ABSCISSE_PAR_DEFAUT=0.0;
	public static final double ORDONNEE_PAR_DEFAUT=0.0;
	
	private double abscisse;
	private double ordonnee;

	
	//Constructors
	public Coordonnees(double abscisse, double ordonnee) {
		this.abscisse=abscisse;
		this.ordonnee=ordonnee;
	}
	
	public Coordonnees() {
		this(ABSCISSE_PAR_DEFAUT,ORDONNEE_PAR_DEFAUT);
	}
	
	
	
	//Accessors and mutators
	public void setAbscisse(double abscisse) {
		this.abscisse=abscisse;
	}
	
	public double getAbscisse() {
		return this.abscisse;
	}
	
	
	public void setOrdonnee(double ordonnee) {
		this.ordonnee=ordonnee;
	}
	
	public double getOrdonnee() {
		return this.ordonnee;
	}
	
	
	//Move from delta
	public void deplacerDe(double deltaX, double deltaY) {
		this.setAbscisse(this.getAbscisse()+deltaX);
		this.setOrdonnee(this.getOrdonnee()+deltaY);
	}
	
	//Move to new position
	public void deplacerVers(double nouvelleAbscisse, double nouvelleOrdonnee) {
		this.setAbscisse(nouvelleAbscisse);
		this.setOrdonnee(nouvelleOrdonnee);
	}
	
	//Distance to coordinate
	public double distanceVers(Coordonnees autreCoordonnees) {
		double a=Math.pow(autreCoordonnees.getAbscisse()-this.getAbscisse(), 2);
		double b=Math.pow(autreCoordonnees.getOrdonnee()-this.getOrdonnee(), 2);
		return Math.sqrt(a+b);
	}
	
	//Angle to coordinate
	public double angleVers(Coordonnees autreCoordonnees) {
		double a= autreCoordonnees.getOrdonnee()-this.getOrdonnee();
		double b= autreCoordonnees.getAbscisse()-this.getAbscisse();
		double angle=Math.atan2(a,b);
		return angle;
	}

	
	//String
	public String toString() {
		DecimalFormat df =new DecimalFormat("0.0#");
		String coordonnees= "("+df.format(getAbscisse())+" , "+df.format(getOrdonnee())+")";
		return coordonnees;
	}
	
}
