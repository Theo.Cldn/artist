package fr.eseo.poo.projet.artiste.controleur.outils;

import javax.swing.event.MouseInputListener;

import java.awt.event.MouseEvent;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;


public abstract class Outil implements MouseInputListener{
	
	private Coordonnees debut;
	private Coordonnees fin;
	private PanneauDessin panneauDessin;
	
	
	//Accessors and mutators
	public Coordonnees getDebut(){
		return this.debut;
	}
	
	public Coordonnees getFin(){
		return this.fin;
	}
	
	public PanneauDessin getPanneauDessin(){
		return this.panneauDessin;
	}

	public void setDebut(Coordonnees debut) {
		this.debut=debut;
	}
	
	public void setFin(Coordonnees fin) {
		this.fin=fin;
	}
	
	public void setPanneauDessin(PanneauDessin panneauDessin){
		this.panneauDessin=panneauDessin;
	}
	
	
	//Defined but don't used
	public void mouseClicked(MouseEvent event){}
	public void mouseDragged(MouseEvent event){}
	public void mouseEntered(MouseEvent event){}
	public void mouseExited(MouseEvent event){}
	public void mouseMoved(MouseEvent event){}
	
	
	//When mouse pressed, we collect the position and define as start position
	public void mousePressed(MouseEvent event) {
		double x= event.getX();
		double y= event.getY();
		Coordonnees debut= new Coordonnees(x,y);
		this.setDebut(debut);
	}
	
	
	//When mouse released, we collect the position and define as end position
	public void mouseReleased(MouseEvent event) {
		double x= event.getX();
		double y= event.getY();
		Coordonnees fin = new Coordonnees(x,y); 
		this.setFin(fin);
	}
	
}
