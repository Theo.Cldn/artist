package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.vue.formes.VueCercle;


public class OutilCercle extends OutilForme{
	
	//Constructor
	public OutilCercle() {
		super();
	}
	
	//Create a VueCercle instance base on position collected from mouse listener
	protected VueCercle creerVueForme() {
		if((getDebut().getAbscisse()!=getFin().getAbscisse()) || (getDebut().getOrdonnee()!=getFin().getOrdonnee())) {
			double largeur= getFin().getAbscisse()-getDebut().getAbscisse();
			double hauteur= getFin().getOrdonnee()-getDebut().getOrdonnee();
			double taille= Math.max(Math.abs(largeur), Math.abs(hauteur));
			
			if(largeur<0) {
				getDebut().deplacerDe(-taille, 0);
			}
			if(hauteur<0) {
				getDebut().deplacerDe(0,-taille);
			}
			
			Cercle cercle= new Cercle(this.getDebut(), Math.abs(taille));
			cercle.setCouleur(getPanneauDessin().getCouleurCourante());
			cercle.setRempli(getPanneauDessin().getModeRemplissage());
			VueCercle vueCercle= new VueCercle(cercle);
			return vueCercle;
		}
		else {
			Cercle cercle= new Cercle(getDebut());
			cercle.setCouleur(getPanneauDessin().getCouleurCourante());
			cercle.setRempli(getPanneauDessin().getModeRemplissage());
			VueCercle vueCercle= new VueCercle(cercle);
			return vueCercle;
		}	
	}
	
}
