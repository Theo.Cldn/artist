package fr.eseo.poo.projet.artiste.controleur.actions;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;


public class ActionEffacer extends AbstractAction{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION= "Tout effacer";
	
	private PanneauDessin panneauDessin;
	
	
	//Constructor
	public ActionEffacer(PanneauDessin panneauDessin) {
		super(NOM_ACTION);
		this.panneauDessin= panneauDessin;
	}
	
	
	//Accessors and mutators
	public void setPanneauDessin(PanneauDessin panneauDessin) {
		this.panneauDessin= panneauDessin;
	}
	
	public PanneauDessin getPanneauDessin() {
		return this.panneauDessin;
	}
	
	
	//Action called after clicking on the clear button
	public void actionPerformed(ActionEvent event) {  
		int n = JOptionPane.showConfirmDialog(panneauDessin, "Voulez-vous tout effacer ?",NOM_ACTION,JOptionPane.YES_NO_OPTION);
		if(n==0) {
			this.getPanneauDessin().getVueFormes().clear();	//Clear the VueForme instances in the drawing board
			this.getPanneauDessin().repaint();
		}
	}

}
