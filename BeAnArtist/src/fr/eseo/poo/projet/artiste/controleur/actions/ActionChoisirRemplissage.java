package fr.eseo.poo.projet.artiste.controleur.actions;

import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;


public class ActionChoisirRemplissage extends AbstractAction{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION= "Remplir";
	
	private PanneauDessin panneauDessin;
	
	
	//Constructor
	public ActionChoisirRemplissage(PanneauDessin panneauDessin) {
		super(NOM_ACTION);
		this.panneauDessin= panneauDessin;
	}
	
	
	//Accessors and mutators
	public void setPanneauDessin(PanneauDessin panneauDessin) {
		this.panneauDessin=panneauDessin;
	}
		
	public PanneauDessin getPanneauDessin() {
		return panneauDessin;
	}
		
	
	//Action called after clicking on the filling button
	public void actionPerformed(ActionEvent event) {
		if(panneauDessin.getModeRemplissage())
			panneauDessin.setModeRemplissage(false);
		else {
			panneauDessin.setModeRemplissage(true);
		}
	}

}
