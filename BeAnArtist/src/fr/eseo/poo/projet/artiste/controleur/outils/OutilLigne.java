package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.formes.VueLigne;


public class OutilLigne extends OutilForme{
	
	
	//Constructor
	public OutilLigne() {
		super();
	}
	
	
	//Create a VueLigne instance base on position collected from mouse listener
	protected VueLigne creerVueForme(){
		if((getDebut().getAbscisse()!=getFin().getAbscisse()) || (getDebut().getOrdonnee()!=getFin().getOrdonnee())) {
			double largeur= getFin().getAbscisse()-getDebut().getAbscisse();
			double hauteur= getFin().getOrdonnee()-getDebut().getOrdonnee();
			Ligne ligne= new Ligne(this.getDebut(), largeur, hauteur);
			ligne.setCouleur(getPanneauDessin().getCouleurCourante());
			VueLigne vueLigne= new VueLigne(ligne);
			return vueLigne;
		}
		else {
			Ligne ligne= new Ligne(getDebut());
			ligne.setCouleur(getPanneauDessin().getCouleurCourante());
			VueLigne vueLigne= new VueLigne(ligne);
			return vueLigne;
		}	
	}
		
}
