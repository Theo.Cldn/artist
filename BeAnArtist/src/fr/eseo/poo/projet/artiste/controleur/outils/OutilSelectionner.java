package fr.eseo.poo.projet.artiste.controleur.outils;

import javax.swing.JOptionPane;

import java.awt.event.MouseEvent;

import fr.eseo.poo.projet.artiste.controleur.actions.ActionSelectionner;
import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;

//Tool to select shape on the drawing board
public class OutilSelectionner extends Outil{
	
	private Forme forme;
	
	//empty constructor
	public OutilSelectionner() {}
	
	
	//Accessors and mutators
	public Forme getFormeSelectionner() {
		return this.forme;
	}
	
	public void setFormeSelectionner(Forme forme) {
		this.forme= forme;
	}
	
	
	//If Select tool chosen and click on drawing board
	//Collect position of the click
	//check in the VueForme list in the reverse direction cause last shape are higher on the drawing board
	//If a VueForme instance contains the point
	//Define the shape as selected shape
	//Display information of the shape
	public void mouseClicked(MouseEvent event) {
		double x= event.getX();
		double y= event.getY();
		Coordonnees inOrOut = new Coordonnees(x,y);
		for(int i=getPanneauDessin().getVueFormes().size()-1; i>=0; i--) {
			VueForme vueForme= getPanneauDessin().getVueFormes().get(i);
			if(vueForme.getForme().contient(inOrOut)) {
				this.setFormeSelectionner(vueForme.getForme());
				JOptionPane.showMessageDialog(getPanneauDessin(), vueForme.getForme().toString(),
						ActionSelectionner.NOM_ACTION, JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		}
	}

}
