package fr.eseo.poo.projet.artiste.controleur.actions;

import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.util.List;

import javax.swing.AbstractAction;
import javax.xml.xpath.XPathExpressionException;

import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;
import fr.eseo.poo.projet.artiste.xml.LecteurSVG;


public class ActionImport extends AbstractAction{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION= "Import";
	
	private PanneauDessin panneauDessin;
	
	
	//Constructor
	public ActionImport(PanneauDessin panneauDessin) {
		super(NOM_ACTION);
		this.panneauDessin=panneauDessin;
	}
	
	
	//Accessors and mutators
	public void setPanneauDessin(PanneauDessin panneauDessin) {
		this.panneauDessin= panneauDessin;
	}
	
	public PanneauDessin getPanneauDessin() {
		return this.panneauDessin;
	}
	
	
	//Action called after clicking the import button
	public void actionPerformed(ActionEvent event){
		LecteurSVG lecteur = new LecteurSVG();
		try {
			final List<VueForme> dessin = lecteur.lisDessin("Dessin-in.xml");
			this.getPanneauDessin().getVueFormes().clear();	//Clear the VueForme instances in the drawing board
			this.getPanneauDessin().repaint();
			for (VueForme vueForme : dessin) {
				panneauDessin.ajouterVueForme(vueForme);
			}
		} catch (FileNotFoundException | XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}

}
