package fr.eseo.poo.projet.artiste.controleur.actions;

import javax.swing.AbstractAction;
import javax.swing.JColorChooser;

import java.awt.Color;
import java.awt.event.ActionEvent;

import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;


public class ActionChoisirCouleur extends AbstractAction{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION= "Couleur";
	
	private PanneauDessin panneauDessin;
	
	
	//Constructor
	public ActionChoisirCouleur(PanneauDessin panneauDessin) {
		super(NOM_ACTION);
		this.panneauDessin= panneauDessin;
	}
	
	
	//Accessors and mutators
		public void setPanneauDessin(PanneauDessin panneauDessin) {
			this.panneauDessin=panneauDessin;
		}
		
		public PanneauDessin getPanneauDessin() {
			return panneauDessin;
		}
		
		
		//Action called after clicking on the color button
	public void actionPerformed(ActionEvent event) { //Opening of the dialog to choose a color
		Color couleurChoisie = JColorChooser.showDialog(panneauDessin, "Couleur", Forme.COULEUR_PAR_DEFAUT);//Oracle
		if(couleurChoisie!=null) {
			panneauDessin.setCouleurCourante(couleurChoisie); //Define the choosen color to the current color of the drawing board
		}
	}

}
