package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;
import fr.eseo.poo.projet.artiste.vue.formes.VueEtoile;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;

public class OutilEtoile extends OutilForme{
	
	private final PanneauBarreOutils panneauBarreOutils;
	
	//Constructor
	public OutilEtoile(PanneauBarreOutils panneauBarreOutils) {
		this.panneauBarreOutils=panneauBarreOutils;
	}
	
	
	//Create a VueEtoile instance base on position collected from mouse listener
	protected VueEtoile creerVueForme(){
		if((getDebut().getAbscisse()!=getFin().getAbscisse()) || (getDebut().getOrdonnee()!=getFin().getOrdonnee())) {
			double rayon = this.getDebut().distanceVers(this.getFin());
			double angle = this.getFin().angleVers(this.getDebut());
			Coordonnees coordonnees= new Coordonnees(this.getFin().getAbscisse()-rayon,this.getFin().getOrdonnee()-rayon) ;
		
			Etoile etoile = new Etoile(coordonnees,2*rayon,panneauBarreOutils.getNbBranches(),angle,
					panneauBarreOutils.getLongueurBranche());
			
			etoile.setCouleur(getPanneauDessin().getCouleurCourante());
			etoile.setRempli(getPanneauDessin().getModeRemplissage());
			VueEtoile vueEtoile= new VueEtoile(etoile);
			return vueEtoile;
		}
		else {
			Etoile etoile= new Etoile(getDebut());
			etoile.setCouleur(getPanneauDessin().getCouleurCourante());
			etoile.setRempli(getPanneauDessin().getModeRemplissage());
			VueEtoile vueLigne= new VueEtoile(etoile);
			return vueLigne;
		}	
	}

}
