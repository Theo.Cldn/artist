package fr.eseo.poo.projet.artiste.controleur.actions;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.List;

import javax.swing.AbstractAction;

import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;
import fr.eseo.poo.projet.artiste.xml.EnregistreurSVG;


public class ActionExport extends AbstractAction{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION= "Export";
	
	private PanneauDessin panneauDessin;
	
	
	//Constructor
	public ActionExport(PanneauDessin panneauDessin) {
		super(NOM_ACTION);
		this.panneauDessin=panneauDessin;
	}
	
	
	//Accessors and mutators
	public void setPanneauDessin(PanneauDessin panneauDessin) {
		this.panneauDessin= panneauDessin;
	}
	
	public PanneauDessin getPanneauDessin() {
		return this.panneauDessin;
	}
	
	
	//Action called after clicking the export button
	public void actionPerformed(ActionEvent event){
		EnregistreurSVG enregistreur = new EnregistreurSVG();
		try {
			List<VueForme> dessin = panneauDessin.getVueFormes();
			enregistreur.enregistreDessin("Dessin-out.xml", dessin);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}

}
