package fr.eseo.poo.projet.artiste.controleur.actions;

import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

import fr.eseo.poo.projet.artiste.controleur.outils.OutilLigne;
import fr.eseo.poo.projet.artiste.controleur.outils.OutilEllipse;
import fr.eseo.poo.projet.artiste.controleur.outils.OutilEtoile;
import fr.eseo.poo.projet.artiste.controleur.outils.OutilCercle;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;


public class ActionChoisirForme extends AbstractAction{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION_LIGNE= "Ligne";
	public static final String NOM_ACTION_ELLIPSE= "Ellipse";
	public static final String NOM_ACTION_CERCLE= "Cercle";
	public static final String NOM_ACTION_ETOILE= "Etoile";
	
	private PanneauDessin panneauDessin;
	private PanneauBarreOutils panneauBarreOutils;
	private String action;
	
	
	//Constructor  //super(NOM_ACTION) cause extends AbstractAction and depends on the action
	public ActionChoisirForme(PanneauDessin panneauDessin, PanneauBarreOutils panneauBarreOutils, String action) {
		super(action);
		this.panneauDessin= panneauDessin;
		this.panneauBarreOutils= panneauBarreOutils;
		this.action= action;
	}
	
	
	//Accessors and mutators
	public void setPanneauDessin(PanneauDessin panneauDessin) {
		this.panneauDessin=panneauDessin;
	}
	
	public PanneauDessin getPanneauDessin() {
		return this.panneauDessin;
	}
	
	public void setPanneauBarreOutils(PanneauBarreOutils panneauBarreOutils) {
		this.panneauBarreOutils=panneauBarreOutils;
	}
	
	public PanneauBarreOutils getPanneauBarreOutils() {
		return this.panneauBarreOutils;
	}
	
	public void setAction(String action) {
		this.action= action;
	}
	
	public String getAction() {
		return this.action;
	}
	
	
	//if action = String Forme
	//Association of the corresponding OutilForme
	public void actionPerformed(ActionEvent event) {
		if(this.getAction()=="Ligne") {
			getPanneauDessin().associerOutil(new OutilLigne());
		}
		if(this.getAction()=="Ellipse") {
			getPanneauDessin().associerOutil(new OutilEllipse());
		}
		if(this.getAction()=="Cercle") {
			getPanneauDessin().associerOutil(new OutilCercle());
		}
		if(this.getAction()=="Etoile") {
			getPanneauDessin().associerOutil(new OutilEtoile(getPanneauBarreOutils()));
		}
	}
	
}
