package fr.eseo.poo.projet.artiste.controleur.actions;

import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

import fr.eseo.poo.projet.artiste.controleur.outils.OutilSelectionner;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;


public class ActionSelectionner extends AbstractAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION= "Selectionner";
	
	private PanneauDessin panneauDessin;
	
	
	//Constructor
	public ActionSelectionner(PanneauDessin panneauDessin) {
		super(NOM_ACTION);
		this.panneauDessin=panneauDessin;
	}
	
	
	//Accessors and mutators
	public void setPanneauDessin(PanneauDessin panneauDessin) {
		this.panneauDessin= panneauDessin;
	}
	
	public PanneauDessin getPanneauDessin() {
		return this.panneauDessin;
	}
	
	
	//Action called after clicking on the select button
	public void actionPerformed(ActionEvent event){
		OutilSelectionner outil= new OutilSelectionner();
		getPanneauDessin().associerOutil(outil);
		}
}
