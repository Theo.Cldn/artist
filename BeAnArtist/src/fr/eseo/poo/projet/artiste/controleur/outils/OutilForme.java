package fr.eseo.poo.projet.artiste.controleur.outils;

import java.awt.event.MouseEvent;

import fr.eseo.poo.projet.artiste.vue.formes.VueForme;

//An OutilForme is a tool but all tools are not OutilForme !!
public abstract class OutilForme extends Outil{
	
	//When mouse pressed 2 times, default from will be printed
	public void mouseClicked(MouseEvent event) {
		super.mouseClicked(event);
		if(event.getClickCount()==2) {
			getPanneauDessin().ajouterVueForme(creerVueForme());
			getPanneauDessin().repaint();
		}
	}
	
	//Nothing to do with the drag method
	public void mouseDragged(MouseEvent event) {}
	
	
	//When mouse released, collect of position and create the associate shape depending of the tools used
	public void mouseReleased(MouseEvent event) {
		super.mouseReleased(event);
		if((getDebut().getAbscisse()!=getFin().getAbscisse()) || (getDebut().getOrdonnee()!=getFin().getOrdonnee())) {
			getPanneauDessin().ajouterVueForme(creerVueForme());
			getPanneauDessin().repaint();
		}
	}
	
	//Abstract cause depends on the shape that will be created
	protected abstract VueForme creerVueForme();
	
}
