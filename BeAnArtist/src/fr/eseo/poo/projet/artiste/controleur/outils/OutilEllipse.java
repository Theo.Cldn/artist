package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;
import fr.eseo.poo.projet.artiste.vue.formes.VueEllipse;


public class OutilEllipse extends OutilForme{
	
	//Constructor
	public OutilEllipse() {
		super();
	}
	
	//Create a VueEllipse instance base on position collected from mouse listener
	protected VueEllipse creerVueForme() {
		if((getDebut().getAbscisse()!=getFin().getAbscisse()) || (getDebut().getOrdonnee()!=getFin().getOrdonnee())) {
			double largeur= Math.abs(getFin().getAbscisse()-getDebut().getAbscisse());
			double hauteur= Math.abs(getFin().getOrdonnee()-getDebut().getOrdonnee());
			double x= Math.min(getDebut().getAbscisse(), getFin().getAbscisse());
			double y= Math.min(getDebut().getOrdonnee(), getFin().getOrdonnee());
			setDebut(new Coordonnees(x,y));
			Ellipse ellipse= new Ellipse(this.getDebut(), Math.abs(largeur), Math.abs(hauteur));
			ellipse.setCouleur(getPanneauDessin().getCouleurCourante());
			ellipse.setRempli(getPanneauDessin().getModeRemplissage());
			VueEllipse vueEllipse= new VueEllipse(ellipse);
			return vueEllipse;
		}
		else {
			Ellipse ellipse= new Ellipse(getDebut());
			ellipse.setCouleur(getPanneauDessin().getCouleurCourante());
			ellipse.setRempli(getPanneauDessin().getModeRemplissage());
			VueEllipse vueEllipse= new VueEllipse(ellipse);
			return vueEllipse;
		}		
	}

}
