package fr.eseo.poo.projet.artiste.xml;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import javax.xml.xpath.XPathExpressionException;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;

/**
 * The SVG Saver is accounting for saving the drawing in the SVG format.
 * 
 * It uses classes java.io.FileWriter and java.io.Writer of the Java API standard that help writing in a text file
 *
 */
public class EnregistreurSVG {

	/**
	 * Start the saving test (teste method) with the input SVG file "Dessin-in.svg" and the output SVG file "Dessin-out.svg". 
	 * 
	 * Verify the conformity of the output file by displaying it with a WEB navigator and comparing the display with the one 
	 * given by the method LecteurSVG.main
	 */
	public static void main(String[] args) throws IOException, XPathExpressionException {
		EnregistreurSVG enregistreur = new EnregistreurSVG();
		enregistreur.teste("Dessin-in1.xml", "Dessin-in2.xml");
	}

	/**
	 * Start the saving of the drawing into a SVG file. The input SVG file is read then save in an output file.
	 * The output file is finally load and display by the app
	 *
	 * @param nomFichierEntrée the input SVG file
	 * @param nomFichierSortie the output SVG file
	 * @throws FileNotFoundException if one of the file is not read
	 */
	public void teste(String nomFichierEntrée, String nomFichierSortie) throws IOException, XPathExpressionException {
		LecteurSVG lecteur = new LecteurSVG();
		List<VueForme> dessin = lecteur.lisDessin(nomFichierEntrée);
		enregistreDessin(nomFichierSortie, dessin);
		lecteur.teste(nomFichierSortie);
	}
	
	/**
	 * Save the drawing given in a file.
	 * 
	 * @param nomFichier Save file name
	 * @param dessin The drawing containing view of the shape
	 * @throws IOException if writing failed
	 */
	public void enregistreDessin(String nomFichier, List<VueForme> dessin) throws IOException {
		Writer redacteur = new FileWriter(nomFichier);
		redacteur.write("<?xml version='1.0' encoding='UTF-8' ?>\r\n");
		ecrisDessin(dessin, redacteur);
		redacteur.close();
	}
	/*
	Ecrit l'entête du document xml comme <DOCTYPE html> pour html.
	Lance le processus d'enregistrement des formes par la fonction ecrisDessin.
	 */

	/**
	 * Write each shape of the drawing
	 * 
	 * @param dessin Drawing to write
	 * @param redacteur SVG writer
	 * @throws IOException if writing failed
	 */
	public void ecrisDessin(List<VueForme> dessin, Writer redacteur) throws IOException {
		redacteur.write("<svg xmlns='http://www.w3.org/2000/svg'>\r\n");
		for (VueForme vueForme : dessin) {
			ecrisForme(vueForme.getForme(), redacteur);
		}
		redacteur.write("</svg>");
	}
	/*
	Ecrit l'entête svg pour signaler que l'on passe en svg.
	Fonction qui écrit les formes de la liste de formes vuFormes dans le document svg.
	Les formes etant différentes il faut étudier de quelle forme il s'agit.
	Suivant le type de forme on appelera des fontions en fonction de la forme.
	
	</svg> a la fin du document pour fermer la balise <svg>
	 */

	
	/**
	 * Write the shape. This method invoke methods ecris<Forme> depending on the type of the shape.
	 * 
	 * @param dessin Drawing to write
	 * @param redacteur SVG writer
	 * @throws IOException if writing failed
	 */
	public void ecrisForme(Forme forme, Writer redacteur) throws IOException {
		String nom = forme.getClass().getSimpleName();
		switch (nom) {
			case "Ellipse":
				nom = "ellipse";
				break;
			case "Cercle":
				nom = "circle";
				break;
			case "Ligne":
				nom = "line";
				break;
			case "Etoile":
				nom = "polygon";
				break;
		}
		redacteur.write("\t<" + nom + " ");
		switch (nom) {
			case "ellipse":
				ecrisEllipse((Ellipse) forme, redacteur);
				break;
			case "circle":
				ecrisCercle((Cercle) forme, redacteur);
				break;
			case "line":
				ecrisLigne((Ligne) forme, redacteur);
				break;
			case "polygon":
				ecrisEtoile((Etoile) forme, redacteur);
				break;
		}
		// définition de la couleur de la forme
		Color c = forme.getCouleur();
		String couleur = String.format("#%02x%02x%02x", c.getRed(), c.getGreen(), c.getBlue());
		redacteur.write("stroke='" + couleur +"' ");
		redacteur.write("/>\r\n");
	}

	/*
	Recupère les formes et analyse le type de la forme
	Si le nom de la forme est ellipse on lancera la focntion ecrisEllipse
	etc.
	Si le nom est polygon on lancera la focntion ecrisEtoile car la forme Etoile n'existe pas en xml
	il s'agit d'un ploygone particulier
	
	On recupere aussi la couleur de la forme que l'on enregistre en utilisant stroke=color
	/>\n pour fermer la balise et retourner à la ligne 
	 */
	
	
	/**
	 * Write the ellipse
	 * 
	 * @param forme shape to write
	 * @param redacteur SVG writer
	 * @throws IOException if writing failed
	 */
	public void ecrisEllipse(Ellipse forme, Writer redacteur) throws IOException {
		double rx= forme.getLargeur() / 2;
		double ry= forme.getHauteur() / 2;
		double cx= forme.getPosition().getAbscisse()+rx;
		double cy= forme.getPosition().getOrdonnee()+ry;
		boolean rempli = forme.estRempli();
		redacteur.write("cx='"+cx+"' cy='"+cy+"' rx='"+rx+"' ry='"+ry+"' ");
		if (rempli){
			Color c = forme.getCouleur();
			String couleur = String.format("#%02x%02x%02x", c.getRed(), c.getGreen(), c.getBlue());
			redacteur.write("fill='" + couleur +"' ");
			}
	}
	/*
	Ecris une ellipse en récupérant les valeurs de l'objet Ellipse, svg de la forme : 
	 <ellipse cx='...' cy='...' rx='...' ry='...'
	 puis la fonction ecrisForme se chargera de rajouter fill='none' stroke='color />\n pour le retour a la ligne
	 */

	/**
	 * Write the circle
	 * 
	 * @param forme shape to write
	 * @param redacteur SVG writer
	 * @throws IOException if writing failed
	 */
	public static void ecrisCercle(Cercle forme, Writer redacteur) throws IOException {
		double r= forme.getLargeur() / 2;
		double cx= forme.getCadreMinX()+r;
		double cy= forme.getCadreMinY()+r;
		boolean rempli = forme.estRempli();
		redacteur.write("cx='"+cx+"' cy='"+cy+"' r='"+r+"' ");
		if (rempli){
			Color c = forme.getCouleur();
			String couleur = String.format("#%02x%02x%02x", c.getRed(), c.getGreen(), c.getBlue());
			redacteur.write("fill='" + couleur +"' ");
			}
	}
	/*
	Ecris un cercle en récupérant les valeurs de l'objet Cercle, svg de la forme : 
	 <circle cx='...' cy='...' r='...'
	 puis la fonction ecrisForme se chargera de rajouter fill='none' stroke='color />\n pour le retour a la ligne
	 */

	/**
	 * Write the line
	 * 
	 * @param forme shape to write
	 * @param redacteur SVG writer
	 * @throws IOException if writing failed
	 */
	public static void ecrisLigne(Ligne forme, Writer redacteur) throws IOException {
		double x1= forme.getC1().getAbscisse();
		double y1= forme.getC1().getOrdonnee();
		double x2= forme.getC2().getAbscisse();
		double y2= forme.getC2().getOrdonnee();
		redacteur.write("x1='"+x1+"' y1='"+y1+"' x2='"+x2+"' y2='"+y2+"' ");
	}
	/*
	Ecris une ligne en récupérant les valeurs de l'objet Ligne, svg de la forme :
	 <line x1='..'. y1='...' x2='...' y2='...'
	 puis la fonction ecrisForme se chargera de rajouter fill='none' stroke='color />\n pour le retour a la ligne
	 
	 */
	/**
	 * Write the star
	 * 
	 * @param forme shape to write
	 * @param redacteur SVG writer
	 * @throws IOException if writing failed
	 */
	public void ecrisEtoile(Etoile forme, Writer redacteur) throws IOException {
		redacteur.write("points='");
		for (Coordonnees point : forme.getCoordonnees()) {
			redacteur.write(point.getAbscisse()+","+point.getOrdonnee()+" ");
		}
		redacteur.write("' ");
		boolean rempli = forme.estRempli();
		if (rempli){
			Color c = forme.getCouleur();
			String couleur = String.format("#%02x%02x%02x", c.getRed(), c.getGreen(), c.getBlue());
			redacteur.write("fill='" + couleur +"' ");
			}
	}
	/*
	Ecris un polygon en récupérant les valeurs de l'objet Etoile, svg de la forme : 
	 <polygon points=..., ..., ..., ..., ... etc.
	 puis la fonction ecrisForme se chargera de rajouter fill='none' stroke='color />\n pour le retour a la ligne
	 */

}
