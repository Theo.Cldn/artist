package fr.eseo.poo.projet.artiste.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Handle the charging process of a XML file into a DOM file
 */
public class ChargeurDOM {

	/**
	 * Load a XML file with the given name and provide with the corresponding DOM file following the DOM standard
	 * 
	 * If the file is not find, null value will be given back.
	 * 
	 * @param nomFichier file's name.
	 * @return DOM document representing the file.
	 */
	public Document charge(String nomFichier) {
		try {
			DocumentBuilder constructeur = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			File fichier = new File(nomFichier);
			return constructeur.parse(fichier);
		}
		catch (ParserConfigurationException | SAXException | IOException e) {
			return null;
		}
	}

}
