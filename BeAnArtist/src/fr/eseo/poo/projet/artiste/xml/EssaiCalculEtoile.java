package fr.eseo.poo.projet.artiste.xml;

import java.awt.Color;

import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;
import fr.eseo.poo.projet.artiste.vue.formes.VueEtoile;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

public class EssaiCalculEtoile {

	//Examples of 3 stars
	static final Etoile ETOILE_A = new Etoile(new Coordonnees(50, 70), 200, 3, 30. * Math.PI / 180., 0.75);
	static final Etoile ETOILE_B = new Etoile(new Coordonnees(100, 100), 200, 4, 0, 0.65);
	static final Etoile ETOILE_C = new Etoile(new Coordonnees(100, 100), 200, 5, 30. * Math.PI / 180., 0.75);

	public static void main(String[] args) {
		// 1 star is created, 1 star is retrieve from the xml file : They shall be superposed
		Etoile etoile1 = ETOILE_A;
		Etoile etoile2 = new LecteurSVG().calculeEtoile(etoile1.getCoordonnees());
		System.out.println(etoile1.getCoordonnees());
		System.out.println(etoile2.getCoordonnees());
		etoile1.setCouleur(Color.RED);
		etoile2.setCouleur(Color.BLUE);
		JFrame fenetre = new JFrame("Etoile");
		PanneauDessin panneauDessin = new PanneauDessin(960, 720, new Color(255, 255, 255));
		fenetre.add(panneauDessin);
		fenetre.pack();
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setLocationRelativeTo(null);
		panneauDessin.ajouterVueForme(new VueEtoile(etoile1));
		panneauDessin.ajouterVueForme(new VueEtoile(etoile2));
		fenetre.setVisible(true);
	}

}
