package fr.eseo.poo.projet.artiste.xml;

import static javax.xml.xpath.XPathConstants.NODESET;
import static javax.xml.xpath.XPathConstants.NUMBER;
import static javax.xml.xpath.XPathConstants.STRING;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.formes.VueCercle;
import fr.eseo.poo.projet.artiste.vue.formes.VueEllipse;
import fr.eseo.poo.projet.artiste.vue.formes.VueEtoile;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.formes.VueLigne;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;
import fr.eseo.poo.projet.artiste.xml.ChargeurDOM;

/**
 * A SVG Reader is a DOM/XPath process accounting the load of a draw with the SVG format.
 */
public class LecteurSVG {

	/**
	 * Start the loading test (teste method) with the SVG file
	 */
	public static void main(String[] args) throws FileNotFoundException, XPathExpressionException {
		LecteurSVG lecteur = new LecteurSVG();
		lecteur.teste("Dessin-in.xml");
	}

	
	
	/**
	 * Evaluator of XPath expressions helping to easily navigate in the DOM file.
	 */
	private XPath xpath = XPathFactory.newInstance().newXPath();

	/**
	 * Test the loading of the SVG file. The file's content is next display in the application window.
	 * 
	 * @param nomFichier file to read
	 * @throws FileNotFoundException if the file doesn't exist
	 * @throws XPathExpressionException
	 */
	public void teste(String nomFichier) throws FileNotFoundException, XPathExpressionException {
		final List<VueForme> dessin = lisDessin(nomFichier);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// initialization of the drawing board
				JFrame fenetre = new JFrame("Etre Un Artiste");
				PanneauDessin panneauDessin = new PanneauDessin(700, 400, new Color(255, 255, 255));
				fenetre.add(panneauDessin);
				fenetre.pack();
				fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				fenetre.setLocationRelativeTo(null);
				// Add shape to the drawing board
				for (VueForme vueForme : dessin) {
					panneauDessin.ajouterVueForme(vueForme);
				}
				fenetre.setVisible(true);
			}
		});
	}

	
	
	/**
	 * Load the SVG file in a DOM file and return the totality of the draw in the list of view representing all shapes stored in the file
	 * 
	 * @param nomFichier SVG file
	 * @return list of view containing the draw
	 * @throws FileNotFoundException if the file is not found or not accessible
	 * @throws XPathExpressionException
	 */
	public List<VueForme> lisDessin(String nomFichier) throws FileNotFoundException, XPathExpressionException {
		List<VueForme> dessin = new ArrayList<>();
		Document document = new ChargeurDOM().charge(nomFichier);
		NodeList figures = (NodeList) xpath.evaluate(".//svg/*", document, NODESET);
		for (int i = 0; i < figures.getLength(); i++) {
			Node noeud = figures.item(i);
			VueForme vue = lisVueForme(noeud);
			if (vue != null) {
				dessin.add(vue);
			}
		}
		return dessin;
	}
	/*
	Collect all nodes in the file having for root <svg> and navigate nodes and start lisVueForme method for reading 
	the shape of the node to add it to the vueForme list and after, to the draw.
	 */

	
	
	/**
	 * Create a shape and his associated view representing by the DOM node given, then return the view.
	 * This method invoke methods lis<Forme> defined for each of <Forme> considered.
	 * 
	 * @param noeud node representing the shape and the view
	 * @return the view stored in the considered node
	 */
	public VueForme lisVueForme(Node noeud) throws XPathExpressionException {
		VueForme vue = null;
		switch (noeud.getNodeName()) {
			case "ellipse":
				vue = new VueEllipse(lisEllipse(noeud));
				break;
			case "circle":
				vue = new VueCercle(lisCercle(noeud));
				break;
			case "line":
				vue = new VueLigne(lisLigne(noeud));
				break;
			case "polygon":
				vue = new VueEtoile(lisEtoile(noeud));
				break;
		}
		if(vue!=null) {
			Color couleur = lisCouleur(noeud);
			vue.getForme().setCouleur(couleur);
		}
		return vue;
	}
	/*
	Lis la forme du noeud
	Si le noeud a pour nom <ellipse ..> on lance la focntion VueEllipse qui récupèrera les valeurs de l'ellipse qui sont stockés
	dans les attributs du noeuds
	on fait pareil pour les nom <circle ...>, <line ...> et <polygon ...>(etoile) 
	
	Puis lit la couleur de chaque forme en lancant la focntion lisCouleur()
	 */

	
	
	/**
	 * Return a new ellipse represented by the give DOM node.
	 * 
	 * @param noeud the ellipse node
	 * @return ellipse stored in the node
	 */
	public Ellipse lisEllipse(Node noeud) throws XPathExpressionException {
		double cx = (double) xpath.evaluate("@cx", noeud, NUMBER);
		double cy = (double) xpath.evaluate("@cy", noeud, NUMBER);
		double rx = (double) xpath.evaluate("@rx", noeud, NUMBER);
		double ry = (double) xpath.evaluate("@ry", noeud, NUMBER);
		String rempli = (String) xpath.evaluate("@fill", noeud, STRING);
		Coordonnees position = new Coordonnees(cx-rx,cy-ry);
		Ellipse ellipse = new Ellipse(position, 2*rx, 2*ry);
		if (!rempli.isEmpty()){ellipse.setRempli(true);}
		return ellipse;
	}
	/*
	Si le nom a pour nom ellipse dans ce cas la forme est une ellipse il faut recuperer en utilisant les requêtes
	XPATH les valeurs de la forme Ellipse en spécifiant l'id de type entier
	<ellipse cx='...' cy='...' rx='...' ry='...' <==> <ellipse @cx='...' @cy='...' @rx='...' @ry='...'
	 */

	
	
	/**
	 * Return a new circle represented by the give DOM node.
	 * 
	 * @param noeud the circle node
	 * @return circle stored in the node
	 */
	public Cercle lisCercle(Node noeud) throws XPathExpressionException {
		double cx = (double) xpath.evaluate("@cx", noeud, NUMBER);
		double cy = (double) xpath.evaluate("@cy", noeud, NUMBER);
		double r = (double) xpath.evaluate("@r", noeud, NUMBER);
		String rempli = (String) xpath.evaluate("@fill", noeud, STRING);
		Coordonnees position = new Coordonnees(cx-r,cy-r);
		Cercle cercle = new Cercle(position, 2*r);
		if (!rempli.isEmpty()){cercle.setRempli(true);}
		return cercle;
	}
	/*
	Si le nom a pour nom circle dans ce cas la forme est un cercle il faut recuperer en utilisant les requêtes
	XPATH les valeurs de la forme Cercle en spécifiant l'id de type entier
	 <circle cx='...' cy='...' r='...' <==>  <circle @cx='...' @cy='...' @r='...'
	 */

	
	
	/**
	 * Return a new line represented by the give DOM node.
	 * 
	 * @param noeud the line node
	 * @return line stored in the node
	 */
	public Ligne lisLigne(Node noeud) throws XPathExpressionException {
		double x1 = (double) xpath.evaluate("@x1", noeud, NUMBER);
		double y1 = (double) xpath.evaluate("@y1", noeud, NUMBER);
		double x2 = (double) xpath.evaluate("@x2", noeud, NUMBER);
		double y2 = (double) xpath.evaluate("@y2", noeud, NUMBER);
		Coordonnees p1 = new Coordonnees(x1,y1);
		Coordonnees p2 = new Coordonnees(x2,y2);
		Ligne ligne = new Ligne();
		ligne.setC1(p1);
		ligne.setC2(p2);
		return ligne;
	}
	/*
	Si le nom a pour nom line dans ce cas la forme est une ligne il faut recuperer en utilisant les requêtes
	XPATH les valeurs de la forme Ligne en spécifiant l'id de type entier
	 <line x1='...' y1='...' x2='...' y2='...' <==>  <line @x1='...' @y1='...' @x2='...' @y2='...'
	 on creer une coordonnees p1 et p2 suivant les coordonnées 1 et 2.
	 */

	
	
	/**
	 * Return a new star represented by the give DOM node.
	 * 
	 * @param noeud the star node
	 * @return star stored in the node
	 */
	public Etoile lisEtoile(Node noeud) throws XPathExpressionException {
		String points = (String) xpath.evaluate("@points", noeud, STRING);
		String rempli = (String) xpath.evaluate("@fill", noeud, STRING);
		StringTokenizer tokenizer = new StringTokenizer(points, ", ");
		List<Coordonnees> coordonnees = new ArrayList<>();
		while (tokenizer.hasMoreTokens()) {
			double x = Double.valueOf(tokenizer.nextToken());
			double y = Double.valueOf(tokenizer.nextToken());
			coordonnees.add(new Coordonnees(x, y));
		}
		Etoile etoile = calculeEtoile(coordonnees);
		if (!rempli.isEmpty()){etoile.setRempli(true);}
		return etoile;
	}
	/*
	recupère les valeurs des points stocker dans l'attribut @points séparer par des virgules
	lance la fonction calculeEtoile afin de calculer les différentes variables de etoiles à partir des points
	 */

	
	
	/**
	 * Return a new star calculated with the points list collected before.
	 * 
	 * @param points Points list of the star
	 * @return the corresponding star calculates based on the points list
	 */
	public Etoile calculeEtoile(List<Coordonnees> points) {
		int sommets = points.size()/2;
		double angle = (2*Math.PI)/sommets;
		Coordonnees sommet1 = points.get(0);
		Coordonnees sommet2 = points.get(1);
		Coordonnees sommet3 = points.get(2);
		double distanceGrandSommet = sommet1.distanceVers(sommet3);
		double rayon = (distanceGrandSommet/2)/(Math.sin(angle/2));
		
		Coordonnees oppose = points.get(points.size()/2);
		double angleCentre = sommet1.angleVers(oppose);
		Coordonnees centre = new Coordonnees(sommet1.getAbscisse()+rayon*Math.cos(angleCentre), sommet1.getOrdonnee()+rayon*Math.sin(angleCentre));
		
		Coordonnees cadre = new Coordonnees(centre.getAbscisse()-rayon,centre.getOrdonnee()-rayon);
		double anglePremBranche = centre.angleVers(sommet1);
		double longueurBranche = 1-(centre.distanceVers(sommet2)/centre.distanceVers(sommet1));
		
		Etoile etoile = new Etoile(cadre, 2*rayon, sommets, anglePremBranche, longueurBranche);
		return etoile;
	}

	
	
	/**
	 * Convert a color (string) into a color (compatible with the Java API).
	 * Format "#RRVVBB" where RR is the hex value for red, VV is for green and BB is for blue.
	 * 
	 * @param couleur color "#RRVVBB"
	 * @return color "Java API"
	 */
	public Color lisCouleur(Node noeud) throws XPathExpressionException {
		String couleur = (String) xpath.evaluate("@stroke", noeud, STRING);
		try {
			return Color.decode(couleur);
		}
		catch (NumberFormatException e) {
			return Forme.COULEUR_PAR_DEFAUT;
		}
	}
	/*
	Lis la couleur stocker dans chaque noeud à l'attribut @stroke 
	Si une exception est levée on assigne la couleur par defaut comme couleur de forme
	 */

}
