# Artist

This Java application is part of a school project

# Getting started
To run the application you must intall the Java Runtime Environment (JRE) (I have jre1.8.0_181 but you can take the most recent version).

# What is in this rep ?
In this repo, you can find:
- The source code in the BeAnArtist folder that you can import into Eclipse.
- The Executable Jar File that is the application compiled and can be start launching it.

# How to use the application ?
The application is separated in 2 parts: The drawing board and the tools board

-The Drawing board will hold all the shape you are going to draw.

-The tools board where you can find different tools to:
- draw shape (line, circle, ellipse, star)
- select information's shape
- color shape
- fill shape with a color
- choose the number of star branch
- choose the length of star branch
- import a drawing 
- export a drawing

# Import a drawing
1) You can only import a xml file that use SVG to design shape. (same file as exported file by the app)
2) you have to rename the file "Dessin-in.xml"
3) Import it by selecting the import button

# Export a drawing
1) Draw in the drawing board
2) Select the export by selecting the export button
3) a file name "Dessin-out.xml" will appear
4) you can import it late by renaming "Dessin-in.xml" with the same app or send it to your friend

# Disclaimer to student !!
I am not responsible for any plagiarism that can occur to any student of the school who use the source code to validate the project !!


# Links to understand XML and SVG
https://www.tutorialspoint.com/java_xml/java_dom_parse_document.htm
https://www.snoweb.io/fr/web-design/fichier-svg/
https://examples.javacodegeeks.com/core-java/xml/xpath/java-xpathfactory-example/